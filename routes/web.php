<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/login', 'AuthController@viewLogin');
Route::post('/login', 'AuthController@sendLogin');

Route::get('/logout', 'AuthController@logout');

Route::get('/inscription', 'AuthController@viewRegister');
Route::get('/view-email-confirm', 'AuthController@viewEmailConfirm');
Route::post('/inscription', 'AuthController@sendRegister')->name('register-post');
Route::get('/confirm-email/{token}', 'AuthController@confirmEmail')->name('confirm-email');
Route::post('/confirm-email/{token}', 'AuthController@confirmEmailPost')->name('confirm-email-post');

Route::get('/reset/password', 'AuthPasswordController@getPasswordResetView')->name('reset-password');
Route::post('/reset/password/mail', 'AuthPasswordController@sendResetLink')->name('reset-password-email');
Route::get('/reset/password/{token}', 'AuthPasswordController@getPasswordUpdateView')->name('reset-password-token');
Route::post('/reset/password/{token}', 'AuthPasswordController@updatePassword')->name('reset-password-validate');

Route::get('/calendar', 'HomeController@viewCalendar')->name('calendar');

Route::middleware(['auth'])->group(function() {
    Route::get('/parameters', 'AuthController@viewParameters')->name('parameters');
    Route::post('/parameters', 'AuthController@sendParameters')->name('parameters-post');

    Route::get('/discord-callback', 'AuthController@discordCallback')->name('discord-callback');

    Route::post('/update-skills', 'AuthController@updateSkills')->name('skills-post');

    Route::post('/password-update', 'AuthController@updatePassword')->name('password-update-post');

    Route::get('/jam/create', 'JamController@getJamCreate')->name('jam-create');
    Route::get('/jam/{shortname}/edit', 'JamController@getJamEdit')->name('jam-edit');
    Route::post('/jam/{id}', 'JamController@saveJam')->name('jam-edit-post');
    Route::post('/jam', 'JamController@saveJam')->name('jam-post');

    Route::get('/jam/{shortname}/publish', 'GameController@viewPublish')->name('game-publish');
    Route::post('/game/publish', 'GameController@publishGame')->name('game-publish-post');
    Route::post('/game/delete/{id}', 'GameController@deleteGame')->name('game-delete');

    Route::get('/jam/{shortname}/join', 'JamController@joinJam')->name('jam-join');
    Route::get('/jam/{shortname}/leave', 'JamController@joinJam')->name('jam-leave');

    Route::get('/game/{gameid}/edit', 'GameController@viewEditGame')->name('game-edit');
    Route::post('/game/{gameid}/edit', 'GameController@publishGame')->name('game-edit-post');
    Route::post('/game/{gameid}/notes', 'GameController@saveNotes')->name('game-note-post');

});

Route::get('/games/{username}', 'GameController@playerGames')->name('games-player');

Route::get('/jam/list', 'JamController@getJamList')->name('jam-list');
Route::get('/jam/{shortname}/leaderboard', 'JamController@jamLeaderboard')->name('jam-leaderboard');
Route::get('/jam/{shortname}', 'JamController@getJam')->name('jam');
Route::get('/game/{gameid}', 'GameController@viewGame')->name('game');

