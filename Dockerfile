FROM php:8.1-apache-buster

# Get composer
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel App
WORKDIR /app
COPY . /app
RUN composer install
COPY .env.example .env

# Apache Configuration
COPY docker/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY docker/apache.conf /etc/apache2/conf-available/z-app.conf
RUN a2enconf z-app
RUN a2enmod rewrite

# PHP Configuration
RUN docker-php-ext-install pdo pdo_mysql
COPY docker/php.ini /usr/local/etc/php/conf.d/app.ini

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Fix App rights
RUN chown -R www-data:www-data /app
