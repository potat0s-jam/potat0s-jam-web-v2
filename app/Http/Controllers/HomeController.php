<?php

namespace App\Http\Controllers;

use App\Models\Jam;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function viewCalendar() {
        $jamList = Jam::all();
        return view('jam.planning',
        [
            'jamList' => $jamList
        ]);
    }
}
