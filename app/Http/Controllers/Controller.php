<?php

namespace App\Http\Controllers;

use App\Models\Jam;
use App\Models\Submission;
use App\Models\User;
use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Authenticatable;

    function isAdmin(){
        return $this->isLogged() && $this->loggedUser()->is_admin === 1;
    }

    function isLogged(){
        return $this->loggedUser() != null;
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    function loggedUser(): ?\Illuminate\Contracts\Auth\Authenticatable
    {
        return auth()->guard('web')->user();
    }

    function isJoignable(Jam $jam) : bool {
        return $jam->is_published && new \DateTime() < new \DateTime($jam->end_jam);
    }

    function isPublishable(Jam $jam) : bool {
        return $this->isJoignable($jam) && new \DateTime() > new \DateTime($jam->start_jam);
    }

    function hasJoin(Jam $jam) : bool {
        $user = $this->loggedUser();
        return count(array_filter($jam->submissions, function (Submission $submission) use ($user) {
            return $user != null && $submission->user_id == $user->id;
        })) >= 0;
    }
}
