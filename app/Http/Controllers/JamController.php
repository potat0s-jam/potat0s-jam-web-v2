<?php

namespace App\Http\Controllers;

use App\Models\Jam;
use App\Models\JamCriterion;
use App\Models\JamUser;
use App\Models\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class JamController extends Controller
{
    public function getJamList() {

        $jamList = DB::table('jam');
        if (!$this->isAdmin()) {
            $jamList = $jamList->where('is_published', '=', true);
        }

        $jamList = $jamList->orderBy('start_jam', 'desc');

        $jamList = $jamList->get();

        return view('jam.list', [
            'jamList' => $jamList
        ]);
    }

    public function joinJam(string $shortname)
    {
        $jam = Jam::where('url_title', '=' ,$shortname)->first();
        if ($jam == null) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        } else {
            $jamUser = JamUser::where('jam_id', $jam->id)->where('user_id', $this->loggedUser()->id)->first();
            if ($jamUser != null) {
                DB::delete('DELETE FROM jam_user WHERE user_id = :uid AND jam_id = :jid', [
                    'uid' => $this->loggedUser()->id,
                    'jid' => $jam->id
                ]);
                return redirect('/jam/'.$jam->url_title);
            } else {
                $jamUser = new JamUser();
                $jamUser->jam_id = $jam->id;
                $jamUser->user_id = Auth::id();
                $jamUser->save();
                return redirect('/jam/'.$jam->url_title);
            }
        }
    }

    public function getJam(string $title){

        $jam = Jam::with(['submissions' => function ($q) {
            $q->inRandomOrder();
        }, 'jamCriteria'])->where('url_title', '=', $title);
        if (!$this->isAdmin()) {
            $jam = $jam->where('is_published', '=', true);
        }
        $jam = $jam->first();

        if ($jam == null) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        } else {
            $stats = DB::selectOne('SELECT COUNT(DISTINCT submission.user_id) AS nb_submissions,
                                            COUNT(DISTINCT jam_user.user_id) AS nb_users
                    FROM jam
                    LEFT OUTER JOIN submission ON jam.id = submission.jam_id
                    LEFT OUTER JOIN jam_user ON jam.id = jam_user.jam_id
                    WHERE jam.id = :id', ['id' => $jam->id]);
            $jam->nb_submissions = $stats->nb_submissions;
            $jam->nb_users = $stats->nb_users;

            $notes = DB::selectOne('SELECT COUNT(DISTINCT note.user_id, note.submission_id) AS nb_notes
                    FROM note
                    LEFT OUTER JOIN submission s on note.submission_id = s.id
                    WHERE s.jam_id = :id', ['id' => $jam->id]);
            $jam->nb_notes = $notes->nb_notes;

            if ($this->isLogged()) {
                $joinUser = JamUser::where('jam_id', $jam->id)->where('user_id', $this->loggedUser()->id)->first();
                $jam->has_join = $joinUser != null;
            } else {
                $jam->has_join = false;
            }
            return view('jam.view', [
                'jam' => $jam,
                'title' => $jam->title,
                'isJoinable' => $this->isJoignable($jam),
                'isPublishable' => $this->isPublishable($jam)
            ]);
        }
    }

    public function getJamCreate(){
        if (!$this->isAdmin()) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_admin');
        }
        return view('jam.form');
    }

    public function getJamEdit(string $title){

        if (!$this->isAdmin()) {
            return redirect('/jam/'. $title)->with('error', 'potat0s.jam.not_admin');
        }

        $jam = Jam::with(['submissions' => function ($q) {
            $q->inRandomOrder();
        }, 'jamCriteria'])->where('url_title', '=', $title)->first();

        if ($jam == null) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        } else {
            return view('jam.form', [
                'jam' => $jam
            ]);
        }
    }

    public function saveJam(Request $request, $id = null)
    {
        $jam = new Jam();
        if ($id != null) {
            $jam = Jam::find($id);
            if ($jam == null) {
                return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
            }
        }

        if (!$this->isAdmin()) {
            if($jam->id == null){
                return redirect('/jam/list')->with('error', 'potat0s.jam.not_admin');
            } else {
                return redirect('/jam/'. $jam->url_title)->with('error', 'potat0s.jam.not_admin');
            }

        }

        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'theme' => 'nullable',
            'title' => [
                'required',
                'max:100',
                Rule::unique('jam', 'title')->ignore($jam->id)
            ],
            'ranked' => [
                'required',
                Rule::in('1','0')
            ],
            'published' => [
                'required',
                Rule::in('1','0')
            ],
            'start_jam' => 'required|date',
            'end_jam' => 'required|date|after:start_jam',
            'end_vote' => 'required_if:ranked,1|date|after:end_jam',
            'description' => 'nullable',
            'criterias' => 'nullable|array'
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($inputs);
        } else {
            $jam->title = $inputs['title'];
            $jam->url_title = preg_replace('/\W+/', '-', strtolower($inputs['title']));
            $jam->is_ranked = $inputs['ranked'];
            $jam->is_published = $inputs['published'];
            $jam->start_jam = new \DateTime($inputs['start_jam']);
            $jam->end_jam = new \DateTime($inputs['end_jam']);
            if (array_key_exists('theme', $inputs)) {
                $jam->theme = $inputs['theme'];
            }
            if ($inputs['ranked'] != '1') {
                $jam->end_vote = null;
            } else if (array_key_exists('end_vote', $inputs)) {
                $jam->end_vote = new \DateTime($inputs['end_vote']);
            }
            if (array_key_exists('description', $inputs)) {
                $jam->description = $inputs['description'];
            }
            $jam->save();
            $jam->refresh();

            $criteriaIds = [];
            if (array_key_exists('criterias', $inputs)) {
                foreach ($inputs['criterias'] as $criteria) {
                    $criteria = json_decode($criteria);

                    if (array_key_exists('id', (array) $criteria)) {
                        $jamCriteria = JamCriterion::find($criteria->id);
                        $jamCriteria->id = $criteria->id;
                        $criteriaIds[] = $criteria->id;
                    } else {
                        $jamCriteria = new JamCriterion();
                    }
                    $jamCriteria->name = $criteria->name;
                    $jam->jamCriteria()->save($jamCriteria);
                    $criteriaIds[] = $jamCriteria->id;
                }
            }
            JamCriterion::whereNotIn('id', $criteriaIds)->where('id_jam', $jam->id)->delete();

            return redirect('/jam/'.$jam->url_title.'/edit')->with('success', 'potat0s.jam.update.success');
        }
    }

    public function jamLeaderboard(string $title){

        $jam = Jam::with(['submissions' => function ($q) {
            $q->inRandomOrder();
        }, 'jamCriteria'])->where('url_title', '=', $title);
        if (!$this->isAdmin()) {
            $jam = $jam->where('is_published', '=', true);
        }
        $jam = $jam->first();

        if ($jam == null) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        } else if(!$jam->is_ranked) {
            return redirect('/jam/' . $title)->with('error', 'potat0s.jam.not_ranked');
        } else if (new \DateTime($jam->end_vote) >= new \DateTime()) {
            return redirect('/jam/'.$title)->with('error', 'potat0s.jam.vote_not_over');
        } else {
            $games = Submission::where('jam_id', '=', $jam->id)->get();
            $notes = DB::select('SELECT note.criteria_id, submission.id as submission_id, SUM(note.note)/COUNT(DISTINCT note.user_id) AS note_total
                                from submission
                                INNER JOIN note ON submission.id = note.submission_id
                                WHERE submission.jam_id = :jam_id GROUP BY submission.id, note.criteria_id', [
                'jam_id' => $jam->id
            ]);
            $leaderboard = [];
            foreach ($games as $game){
                $gameLeaderboard = [
                    'submission_id' => $game->id,
                    'title' => $game->title,
                    'url_itchio' => $game->url_itchio,
                    'route' => route('game', [$game->id])
                ];
                $gameNotes = array_filter($notes, function($note, $index) use ($game) {
                    return $note->submission_id == $game->id;
                }, ARRAY_FILTER_USE_BOTH);
                if(count($gameNotes) > 0){
                    $gameNoteByCriteria = [];
                    foreach ($gameNotes as $gameNote){
                        $gameNoteByCriteria[$gameNote->criteria_id] = new class($gameNote) {
                            public $note_total;
                            public $submission_id;

                            public function __construct($gameNote)
                            {
                                $this->note_total = round(floatval($gameNote->note_total), 2);
                                $this->submission_id = $gameNote->submission_id;
                            }
                        };
                    }

                    $gameLeaderboard['leaderboard'] = $gameNoteByCriteria;
                    $gameLeaderboard['leaderboard']["overall"] = new class($gameNotes, $game->id) {
                        public $note_total;
                        public $submission_id;

                        public function __construct($gameNotes, $gameId)
                        {
                            $this->note_total = round(array_sum(array_column($gameNotes, 'note_total')) / count($gameNotes), 2);
                            $this->submission_id = $gameId;
                        }
                    };

                } else {
                    $criterion = JamCriterion::where('id_jam', $jam->id)->get();
                    $gameNoteByCriteria = [];
                    foreach ($criterion as $criteria) {
                        $gameNoteByCriteria[$criteria->id] = new class($game) {
                            public $note_total;
                            public $submission_id;

                            public function __construct($game)
                            {
                                $this->note_total = round(floatval(-1), 2);
                                $this->submission_id = $game->id;
                            }
                        };
                    }

                    $gameLeaderboard['leaderboard'] = $gameNoteByCriteria;
                    $gameLeaderboard['leaderboard']["overall"] = new class($game) {
                        public $note_total;
                        public $submission_id;

                        public function __construct($game)
                        {
                            $this->note_total = round(floatval(-1), 2);
                            $this->submission_id = $game->id;
                        }
                    };
                }
                $leaderboard[] = $gameLeaderboard;
            }
            return view('jam.leaderboard', [
                'jam' => $jam,
                'leaderboard' => $leaderboard,
            ]);
        }
    }
}
