<?php

namespace App\Http\Controllers;

use App\Mail\ResetPassword;
use App\Models\PasswordResets;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthPasswordController extends Controller
{
    public function getPasswordResetView(){
        return view('password-reset');
    }

    public function sendResetLink(Request $request){
        $inputs = $request->all();

        if(in_array('admin', $inputs)){
            return redirect('home');
        }

        $validator = Validator::make($inputs, [
            'email' => [
                'required',
                'email',
                'exists:users,email',
                'max:255'
            ]
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput($inputs);
        }

        PasswordResets::where('email',$inputs['email'])->delete();

        $passwordReset = new PasswordResets();
        $passwordReset->email = $inputs['email'];
        $passwordReset->token = Str::uuid();
        $passwordReset->created_at = now();
        $passwordReset->save();

        Mail::to($passwordReset->email)->send(new ResetPassword($passwordReset->token));

        return redirect('/')->with('success','potat0s.session.reset.send.success');
    }

    public function getPasswordUpdateView($token){
        $passwordResets = PasswordResets::where('token', $token)->first();
        if($passwordResets == null){
            return redirect('/')->with('error','potat0s.session.reset.token.unknown');
        }

        return view('password-reset-update', [
            'token' => $passwordResets->token,
            'email' => $passwordResets->email
        ]);
    }

    public function updatePassword(Request $request) {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'email' => 'required|email|exists:users,email|exists:password_resets,email',
            'token' => 'required|string|exists:password_resets,token',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput($inputs);
        }

        $pR = PasswordResets::where('email', $inputs['email'])->where('token', $inputs['token'])->first();

        if($pR == null) {
            return redirect('/')->with('error', 'potat0s.session.invalid_token_email');
        }

        $user = User::where('email', $inputs['email'])->first();
        $user->password = Hash::make($inputs['password']);
        $user->save();

        return redirect('/')->with('success', 'potat0s.session.update_password_success');
    }
}
