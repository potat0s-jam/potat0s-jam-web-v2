<?php

namespace App\Http\Controllers;

use App\Models\Submission;
use App\Models\Note;
use App\Models\Jam;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class GameController extends Controller
{

    public function viewGame(string $gameId)
    {
        $submission = Submission::where('id', $gameId)->first();
        if ($submission == null) {
            return redirect('/')->with('error', 'potat0s.game.not_found');
        }

        $game = Submission::with(['user' => function ($q) {
            $q->select('id', 'name');
        }, 'notes' => function ($q) {
            if(auth()->user() != null) {
                $q->where('user_id','=', auth()->user()->id);
            }
        }, 'jam'])->find($gameId);

        $hasCreated = false;
        if($this->isLogged()) {
            $hasSubmission = $submission::where('user_id', $this->loggedUser()->id)->where('jam_id', $game->jam->id)->first();
            if($hasSubmission != null){
                $hasCreated = true;
            }
        }

        $isNotable = $this->isLogged() && $game->jam->is_ranked
            && strtotime('now') > strtotime($game->jam->end_jam)
            && strtotime('now') < strtotime($game->jam->end_vote)
            && $this->loggedUser()->name != $submission->user->name
            && $hasCreated;

        return view('game.view', [
            'game' => $game,
            'title' => $game->title,
            'notable' => $isNotable
        ]);
    }

    public function viewPublish(string $title){

        $jam = Jam::where('url_title', '=', $title);
        if (!$this->isAdmin()) {
            $jam = $jam->where('is_published', '=', true);
        }
        $jam = $jam->first();

        if ($jam == null) {
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        }
        if(!$this->isAdmin() && strtotime($jam->end_jam) < strtotime('today UTC')) {
            return redirect('/jam/' . $jam->url_title)->with('error', 'potat0s.game.ended');
        }

        $user = $this->loggedUser();

        $game = Submission::where('jam_id', $jam->id)->where('user_id', $user->id)->first();

        if($game != null){
            return redirect('/jam/' . $jam->url_title)->with('error', 'potat0s.game.already_exist');
        }

        return view('game.form', [
            'jam' => $jam
        ]);
    }

    public function viewEditGame(string $gameId) {

        $user = $this->loggedUser();
        if($user == null) {
            return redirect('/')->with('error', 'potat0s.session.unauthorized');
        }
        $game = Submission::with('jam')->where('id', $gameId)->first();
        if ($game == null) {
            return redirect('/')->with('error', 'potat0s.game.not_found');
        } else if($game->user_id != $user->id){
            return redirect('/game/' . $game->id)->with('error', 'potat0s.game.unauthorized');
        }

        return view('game.form', [
            'game' => $game,
            'jam' => $game->jam
        ]);
    }

    public function deleteGame(string $gameId){
        $user = $this->loggedUser();
        if($user == null) {
            return redirect('/')->with('error', 'potat0s.session.unauthorized');
        }
        $game = Submission::where('id', $gameId)->where('user_id', $user->id)->get();

        if($game == null){
            return redirect('/')->with('error', 'potat0s.game.not_found');
        }

        Submission::where('id', $gameId)->delete();

        return redirect('/')->with('success', 'potat0s.game.delete_success');

    }

    public function publishGame(Request $request, string $gameId = null)
    {
        $user = $this->loggedUser();
        if($user == null) {
            return redirect('/')->with('error', 'potat0s.session.unauthorized');
        }
        $game = new Submission();
        if ($gameId != null) {
            $game = Submission::where('id', $gameId)->where('user_id', $user->id)->first();
            if ($game == null) {
                return redirect('/')->with('error', 'potat0s.game.not_found');
            }
        }

        $inputs = $request->all();

        $jam = Jam::select('end_jam')->find($inputs['jam_id']);
        if($jam == null){
            return redirect('/jam/list')->with('error', 'potat0s.jam.not_found');
        }

        if($gameId == null && !$this->isAdmin() && strtotime($jam->end_jam) < strtotime('today UTC')) {
            return redirect('/jam/' . $jam->url_title)->with('error', 'potat0s.game.ended');
        }

        $validator = Validator::make($inputs, [
            'title' => [
                'required',
                'string',
                'max:100',
                Rule::unique('submission', 'title')->ignore($gameId)
            ],
            'url_itchio' => 'required|url',
            'vignette_url' => [
                'required',
                'url',
                function($attribute, $value, $fail) {
                    if(getimagesize($value) == false){
                        $fail(__('validation.potat0s.vignette_invalid'));
                    }
                }
            ],
            'description' => 'required|string',
            'jam_id' => 'required|exists:App\Models\Jam,id'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($inputs);
        } else {
            $game->title = $inputs['title'];
            $game->url_itchio = $inputs['url_itchio'];
            if (array_key_exists('description', $inputs)) {
                $game->description = $inputs['description'];
            }
            $game->user_id = $user->id;
            $game->jam_id = $inputs['jam_id'];
            $game->vignette_url = $inputs['vignette_url'];

            $game->save();

            $game = Submission::with(['user' => function ($q) {
                $q->select('id', 'name');
            }], 'notes')->find($game->id);

            return redirect('/game/' . $game->id)->with('success', 'potat0s.game.updated');
        }
    }

    public function saveNotes(Request $request){
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'notes' => 'required|array|min:1',
            'submission_id' => 'required|exists:App\Models\Submission,id',
            'notes.*.id' => 'int',
            'notes.*.criteria_id' => 'required|exists:App\Models\JamCriterion,id',
            'notes.*.note' => 'required|integer|min:0|max:5'
        ]);

        if($validator->fails()){
            return redirect('/game/' . $inputs['submission_id'])->with('error', 'potat0s.game.note_invalid');
        } else {
            $user = auth()->guard('web')->user();
            foreach ($inputs['notes'] as $inputNote) {
                $result = DB::select('SELECT COUNT(jam_criterion.id) AS is_valid_criteria
                                    FROM jam_criterion
                                    LEFT OUTER JOIN jam ON jam.id = jam_criterion.id_jam
                                    LEFT OUTER JOIN submission ON submission.jam_id = jam.id
                                WHERE submission.id = ?
                                  AND jam_criterion.id = ?', [$inputs['submission_id'], $inputNote['criteria_id']]);

                if($result[0]->is_valid_criteria == 0){
                    return redirect('/game/' . $inputs['submission_id'])->with('error', 'potat0s.game.note_bad_criteria');
                } else {

                    $note = new Note();

                    if(array_key_exists('id', $inputNote)) {
                        $note = Note::find($inputNote['id']);
                    }

                    if($note->id == null) {
                        $note->submission_id = $inputs['submission_id'];
                        $note->criteria_id = $inputNote['criteria_id'];
                        $note->user_id = $user->id;
                    }
                    $note->note = $inputNote['note'];
                    $note->save();
                }
            }

            return redirect('/game/' . $inputs['submission_id'])->with('success', 'potat0s.game.note_success');
        }
    }

    public function playerGames(string $username){

        $user = User::with('submissions')->where('name', $username)->first();

        if($user == null){
            return redirect('/')->with('error', 'potat0s.session.unknown');
        }

        return view('game.player-list',[
            'user' => $user
        ]);
    }
}
