<?php

namespace App\Http\Controllers;

use App\Mail\EmailConfirmation;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    public function viewLogin()
    {
        if ($this->isLogged()) {
            return redirect('home');
        }

        return view('login');
    }

    public function sendLogin()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')], request('remember') == 'on')) {
            return back()->with('success', 'potat0s.session.login_success');
        } else {
            return back()->with('error-modal', 'potat0s.login.failure')->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/')->with('success', 'potat0s.session.logout_success');
    }

    public function viewRegister()
    {
        return view('register');
    }

    public function sendRegister(Request $request){
        $inputs = $request->all();

        if(in_array('admin', $inputs)){
            return redirect('home');
        }

        $validator = Validator::make($inputs, [
            'name' => 'required|unique:users|max:255',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'password_confirmation' => 'required|same:password',
            'avatar' => 'required',
            'want_newsletter' => 'boolean',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($inputs);
        }

        $user = new User();
        $user->name = $inputs['name'];
        $user->email = $inputs['email'];
        $user->password = Hash::make($inputs['password']);
        $user->avatar = $inputs['avatar'];
        $user->want_newsletter = isset($inputs['want_newsletter']) ? $inputs['want_newsletter'] : 0;
        $user->email_token_validation = Str::uuid();
        $user->save();

        Mail::to($user->email)->send(new EmailConfirmation($user->name, $user->email_token_validation));

        return redirect('/')->with('success', 'potat0s.form.register.success');
    }

    public function confirmEmail($emailToken){
        $user = User::where('email_token_validation', $emailToken)->first();
        if($user == null){
            return redirect('/')->with('error', 'potat0s.register.unknown_token');
        }
        return view('email-confirm', [
            'user' => $user,
        ]);
    }

    public function confirmEmailPost(Request $request, $emailToken){

        $user = User::where('email_token_validation', $emailToken)->first();
        if($user == null) {
            return redirect('/')->with('error', 'potat0s.register.unknown_token');
        }

        if (Auth::attempt(['email' => $user->email, 'password' => request('password')], false)) {
            $user->email_token_validation = null;
            $user->email_verified_at = now();
            $user->save();
            return redirect('/')->with('success', 'potat0s.session.login_success');
        } else {
            return redirect('/confirm-email/' . $emailToken)->with('error', 'potat0s.register.invalid_confirmation');
        }

    }

    public function viewEmailConfirm(){
        return view('emails.confirm',[
            'username' => 'Utilisateur',
            'emailToken' => 'UIHJEUHFEUHFIUEHEIVUBEUIVHVFUHEUH'
        ]);
    }

    public function viewParameters(){
        $user = $this->loggedUser();

        $userSkills = array();
        foreach ($user->skills as $skill){
            $userSkills[] = $skill->id;
        }

        $skills = Skill::all();

        return view('parameter', [
            'user' => $user,
            'userSkills' => $userSkills,
            'skills' => $skills
        ]);
    }


    public function sendParameters (Request $request){
        $inputs = $request->all();

        $user = User::find($this->loggedUser()->id);

        $validator = Validator::make($inputs, [
            'name' => 'required|unique:users,name,'.$user->id.'|max:255',
            'avatar' => 'required',
            'want_newsletter' => 'boolean',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($inputs);
        }

        $user->name = $inputs['name'];
        $user->avatar = $inputs['avatar'];
        if($user->avatar != 'discord'){
            $user->discord_url = null;
        }
        $user->want_newsletter = isset($inputs['want_newsletter']) ? $inputs['want_newsletter'] : 0;
        $user->save();

        return redirect('/parameters')->with('success', 'potat0s.form.parameter.success');

    }

    public function discordCallback(Request $request){
        $inputs = $request->all();
        if(isset($inputs['code'])){
            $data = [
                'grant_type'=> 'authorization_code',
                'client_id'=> env('DISCORD_CLIENT_ID'),
                'client_secret'=> env('DISCORD_CLIENT_SECRET'),
                'code'=> $inputs['code'],
                'redirect_uri'=> route('discord-callback'),
                'scope'=> 'identify'
            ];

            $loginURL = 'https://discord.com/api/v' . env('DISCORD_VERSION') . '/oauth2/token';
            $userResponse = Http::asForm()->post($loginURL, $data);

            if($userResponse->successful()){
                $userURL = 'https://discord.com/api/v' . env('DISCORD_VERSION') . '/users/@me';
                $response = Http::withHeaders([
                    'Authorization' => 'Bearer ' . $userResponse['access_token']
                ])->get($userURL);
                if($response->successful()){
                    $avatarUrl = 'https://cdn.discordapp.com/avatars/' . $response['id'] . '/' . $response['avatar'] . '.png';
                    $user = User::find($this->loggedUser()->id);
                    $user->discord_url = $avatarUrl;
                    $user->save();

                    return redirect('/parameters')->with('success', 'potat0s.form.parameter.discord_success');
                }
            }
        }
        return redirect('/parameters')->with('error', 'potat0s.form.parameter.discord_failure');
    }

    public function updateSkills(Request $request){
        $user = $this->loggedUser();
        $inputs = $request->all();

        if(isset($inputs['skills'])){
            $user_skills = array();
            foreach ($inputs['skills'] as $skill){
                $user_skills[] = [
                    'user_id' => $user->id,
                    'skill_id' => $skill
                ];
            }
            DB::table('user_skills')->where('user_id',$user->id)->whereNotIn('skill_id', $inputs['skills'])->delete();
            DB::table('user_skills')->insertOrIgnore($user_skills);
        } else {
            DB::table('user_skills')->where('user_id',$user->id)->delete();
        }
        return redirect('/parameters')->with('success', 'potat0s.form.parameter.skill_success');
    }

    public function updatePassword (Request $request){
        $inputs = $request->all();
        $user = User::find($this->loggedUser()->id);

        $validator = Validator::make($inputs, [
            'actual_password' => [
                'required',
                'min:8',
                function ($attribute, $value, $fail) use ($user) {
                    if (!Hash::check($value, $user->password)) {
                        $fail(__('potat0s.form.parameter.current_pass_invalid'));
                    }
                }
            ],
            'new_password' => 'required|min:8',
            'password_confirmation' => 'required|same:new_password',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput($inputs);
        }

        $user->password = Hash::make($inputs['new_password']);
        $user->save();

        return redirect('/parameters')->with('success', 'potat0s.form.parameter.password_updated');
    }
}
