<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Potat0sMailable
{
    use Queueable, SerializesModels;

    public $passwordToken;

    /**
     * Create a new message instance.
     *
     * @param $passwordToken
     */
    public function __construct($passwordToken)
    {
        $this->passwordToken = $passwordToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): ResetPassword
    {
        return $this->from(env('mail_from_address'), env('mail_from_name'))->subject(__('potat0s.email.reset-password'))->view('emails.reset-password');
    }
}
