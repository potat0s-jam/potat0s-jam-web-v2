<?php


namespace App\Mail;

use Illuminate\Mail\Mailable;

/**
 * Class Potat0sMailable
 * @package App\Mail
 */
class Potat0sMailable extends Mailable
{
    /**
     * Method that fixes NULL reciption
     *
     * @param array|object|string $address
     * @param null $name
     * @param string $property
     * @return $this|Potat0sMailable
     */
    protected function setAddress($address, $name = null, $property = 'to')
    {
        foreach ($this->addressesToArray($address, $name) as $recipient) {
            $recipient = $this->normalizeRecipient($recipient);

            if(!empty($recipient)){
                $this->{$property}[] = [
                    'name' => $recipient->name ?? null,
                    'address' => $recipient->email,
                ];
            }
        }
        return $this;
    }
}
