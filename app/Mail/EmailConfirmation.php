<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailConfirmation extends Potat0sMailable
{
    use Queueable, SerializesModels;

    public $username;
    public $emailToken;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $emailToken)
    {
        $this->username = $username;
        $this->emailToken = $emailToken;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('mail_from_address'), env('mail_from_name'))->subject(__('potat0s.email.confirmation'))->view('emails.confirm');
    }


}
