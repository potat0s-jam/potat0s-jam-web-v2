<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubmissionField
 *
 * @property int $id
 * @property int $submission_id
 * @property int $field_id
 * @property int $content
 * @property Submission $submission
 * @property Field $field
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField whereFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubmissionField whereSubmissionId($value)
 * @mixin \Eloquent
 */
class SubmissionField extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'submission_field';

    /**
     * @var array
     */
    protected $fillable = ['submission_id', 'field_id', 'content'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function submission()
    {
        return $this->belongsTo('App\Models\Submission');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function field()
    {
        return $this->belongsTo('App\Models\Field');
    }
}
