<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PasswordResets
 *
 * @property string $email
 * @property string $token
 * @property string $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets query()
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PasswordResets whereToken($value)
 * @mixin \Eloquent
 */
class PasswordResets extends Model
{
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['email', 'token', 'created_at'];

}
