<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\JamField
 *
 * @property int $jam_id
 * @property int $field_id
 * @property Jam $jam
 * @property Field $field
 * @method static \Illuminate\Database\Eloquent\Builder|JamField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamField query()
 * @method static \Illuminate\Database\Eloquent\Builder|JamField whereFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JamField whereJamId($value)
 * @mixin \Eloquent
 */
class JamField extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'jam_field';

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jam()
    {
        return $this->belongsTo('App\Models\Jam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function field()
    {
        return $this->belongsTo('App\Models\Field');
    }
}
