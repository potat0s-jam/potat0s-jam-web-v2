<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TypeField
 *
 * @property int $id
 * @property string $nom
 * @property Field[] $fields
 * @property-read int|null $fields_count
 * @method static \Illuminate\Database\Eloquent\Builder|TypeField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeField query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeField whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeField whereNom($value)
 * @mixin \Eloquent
 */
class TypeField extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'type_field';

    /**
     * @var array
     */
    protected $fillable = ['nom'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        return $this->hasMany('App\Models\Field');
    }
}
