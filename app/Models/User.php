<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Http;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property boolean $is_admin
 * @property string $avatar
 * @property boolean $want_newsletter
 * @property string $email_token_validation
 * @property string $created_at
 * @property string $updated_at
 * @property string $remember_token
 * @property Jam[] $jams
 * @property News[] $news
 * @property Note[] $notes
 * @property Submission[] $submissions
 * @property UserPlanning[] $userPlannings
 * @property Skill[] $skills
 * @property string $discord_url
 * @property-read int|null $jams_count
 * @property-read int|null $notes_count
 * @property-read int|null $skills_count
 * @property-read int|null $submissions_count
 * @property-read int|null $user_plannings_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDiscordUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailTokenValidation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereWantNewsletter($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'email', 'email_verified_at', 'is_admin', 'avatar', 'want_newsletter', 'discord_url', 'email_token_validation', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jams()
    {
        return $this->belongsToMany('App\Models\Jam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany('App\Models\News');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany('App\Models\Submission');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userPlannings()
    {
        return $this->hasMany('App\Models\UserPlanning');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skills()
    {
        return $this->belongsToMany('App\Models\Skill', 'user_skills');
    }

    public function avatarUrl()
    {
        $avatarUrl = null;
        switch ($this->avatar) {
            case 'discord':
                if(isset($this->discord_url)){
                    $avatarUrl = $this->discord_url;
                }
                break;
            case 'gravatar':
                $avatarUrl = 'https://www.gravatar.com/avatar/' . md5($this->email);
                break;
            default:
                $avatarUrl = null;
        }
        return $avatarUrl;
    }
}
