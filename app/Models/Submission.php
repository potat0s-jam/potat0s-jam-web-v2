<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Submission
 *
 * @property int $id
 * @property int $user_id
 * @property int $jam_id
 * @property string $title
 * @property string $url_itchio
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Jam $jam
 * @property Note[] $notes
 * @property SubmissionField[] $submissionFields
 * @property string|null $vignette_url
 * @property-read int|null $notes_count
 * @property-read int|null $submission_fields_count
 * @method static \Illuminate\Database\Eloquent\Builder|Submission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Submission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Submission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereJamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereUrlItchio($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Submission whereVignetteUrl($value)
 * @mixin \Eloquent
 */
class Submission extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'submission';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'jam_id', 'title', 'url_itchio', 'description', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jam()
    {
        return $this->belongsTo('App\Models\Jam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissionFields()
    {
        return $this->hasMany('App\Models\SubmissionField');
    }
}
