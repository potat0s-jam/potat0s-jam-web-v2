<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Field
 *
 * @property int $id
 * @property int $type_field_id
 * @property string $nom
 * @property string $description
 * @property boolean $is_archived
 * @property TypeField $typeField
 * @property Jam[] $jams
 * @property SubmissionField[] $submissionFields
 * @property-read int|null $jams_count
 * @property-read int|null $submission_fields_count
 * @method static \Illuminate\Database\Eloquent\Builder|Field newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Field newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Field query()
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereIsArchived($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereNom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereTypeFieldId($value)
 * @mixin \Eloquent
 */
class Field extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'field';

    /**
     * @var array
     */
    protected $fillable = ['type_field_id', 'nom', 'description', 'is_archived'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function typeField()
    {
        return $this->belongsTo('App\Models\TypeField');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function jams()
    {
        return $this->belongsToMany('App\Models\Jam', 'jam_field');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissionFields()
    {
        return $this->hasMany('App\Models\SubmissionField');
    }
}
