<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use function Illuminate\Events\queueable;

/**
 * App\Models\JamCriterion
 *
 * @property int $id
 * @property int $id_jam
 * @property string $name
 * @property Jam $jam
 * @property Note[] $notes
 * @property-read int|null $notes_count
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion query()
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion whereIdJam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JamCriterion whereName($value)
 * @mixin \Eloquent
 */
class JamCriterion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jam_criterion';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['id_jam', 'name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jam()
    {
        return $this->belongsTo('App\Models\Jam', 'id_jam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notes()
    {
        return $this->hasMany('App\Models\Note', 'criteria_id');
    }
}
