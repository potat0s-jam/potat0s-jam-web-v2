<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserPlanning
 *
 * @property int $id
 * @property int $user_id
 * @property string $planning_date
 * @property User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning wherePlanningDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserPlanning whereUserId($value)
 * @mixin \Eloquent
 */
class UserPlanning extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_planning';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'planning_date'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
