<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Jam
 *
 * @property int $id
 * @property string $title
 * @property string $theme
 * @property boolean $is_ranked
 * @property string $url_title
 * @property string $description
 * @property string $start_jam
 * @property string $end_jam
 * @property string $end_vote
 * @property boolean $is_published
 * @property JamCriterion[] $jamCriteria
 * @property Field[] $fields
 * @property User[] $users
 * @property Submission[] $submissions
 * @property-read int|null $fields_count
 * @property-read int|null $jam_criteria_count
 * @property-read int|null $submissions_count
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Jam newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jam newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Jam query()
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereEndJam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereEndVote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereIsRanked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereStartJam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereTheme($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Jam whereUrlTitle($value)
 * @mixin \Eloquent
 */
class Jam extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jam';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['title', 'theme', 'is_ranked', 'url_title', 'description', 'start_jam', 'end_jam', 'end_vote', 'is_published'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jamCriteria()
    {
        return $this->hasMany('App\Models\JamCriterion', 'id_jam');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fields()
    {
        return $this->belongsToMany('App\Models\Field', 'jam_field');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany('App\Models\Submission');
    }
}
