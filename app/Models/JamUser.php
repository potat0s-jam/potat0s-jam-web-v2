<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\JamUser
 *
 * @property int $user_id
 * @property int $jam_id
 * @property User $user
 * @property Jam $jam
 * @method static \Illuminate\Database\Eloquent\Builder|JamUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|JamUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|JamUser whereJamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|JamUser whereUserId($value)
 * @mixin \Eloquent
 */
class JamUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jam_user';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jam()
    {
        return $this->belongsTo('App\Models\Jam');
    }
}
