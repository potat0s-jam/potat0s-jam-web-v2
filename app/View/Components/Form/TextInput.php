<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class TextInput extends Component
{
    public $id;
    public $no_label;
    public $inline;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id, bool $no_label = true, bool $inline = true)
    {
        $this->id = $id;
        $this->no_label = $no_label;
        $this->inline = $inline;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.text-input');
    }
}
