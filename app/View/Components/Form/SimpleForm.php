<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class SimpleForm extends Component
{
    public $class;
    public $formUrl;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($formUrl, $class = '')
    {
        $this->class = $class;
        $this->formUrl = $formUrl;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.form.simple-form');
    }
}
