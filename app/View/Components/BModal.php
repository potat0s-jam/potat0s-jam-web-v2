<?php

namespace App\View\Components;

use Illuminate\View\Component;

class BModal extends Component
{

    public $id;
    public $hideFooter;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $id, bool $hideFooter = false)
    {
        $this->id = $id;
        $this->hideFooter = $hideFooter;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.b-modal');
    }
}
