const CalendarInput = {
    name: 'CalendarInput',
    template: `
        <v-calendar
            :attributes="attrs"
            is-expanded
            title-position="left"
            class="custom-calendar"
            is-dark
            v-model="calendarInput">
        <div slot="day-content" slot-scope="{ day }" v-on:click="$emit('calendar-click', day)"
             class="day-content-parent flex flex-col h-full z-10 overflow-hidden">
                <span class="text-center vc-text-sm"
                      :class="{ 'vc-text-gray-600': day.date < dayDate, 'vc-day-content vc-cursor-pointer' : day.date >= dayDate}">
                    {{ day.day }}
                </span>
        </div>
        </v-calendar>`,
    props: {
        label: String,
        value: String,
        showTime: Boolean,
        preferredDateList: Array,
        interactPreference: Boolean,
        isLogged: Boolean,
        isLoading: Boolean
    },
    data: function () {
        return {
            calendarInput: '',
            dayDate: new Date(),
            attrs: [],
            showPreferenceButton: this.interactPreference
        }
    },
    created() {
        this.calendarInput = this.value
        this.dayDate.setHours(0, 0, 0, 0)
    },
    methods: {
        AddSimpleDotEvent(date, label, key, color) {
            this.RemoveDuplicate(key)
            this.attrs.push({
                key: key,
                dot: color,
                popover: {
                    label: label
                },
                dates: date
            })
        },
        AddSimpleHighlightEvent(date, label, key, color) {
            this.RemoveDuplicate(key)
            this.attrs.push({
                key: key,
                highlight: color,
                popover: {
                    label: label
                },
                dates: date
            })
        },
        AddRangeEvent(startDate, endDate, label, key, color) {
            this.RemoveDuplicate(key)
            this.attrs.push({
                key: key,
                highlight: color,
                popover: {
                    label: label
                },
                dates: [
                    {start: startDate, end: endDate}
                ]
            })
        },
        RemoveDuplicate(key) {
            let i
            this.attrs.forEach((e, index) => {
                if (e.key === key) i = index
            })
            if (i !== undefined) {
                this.attrs.splice(i, 1)
            }
        },
        IsPreferred(day) {
            return Object.values(this.preferredDateList).filter(d => d.toDateString() === day.date.toDateString()).length > 0
        }
    }
}
