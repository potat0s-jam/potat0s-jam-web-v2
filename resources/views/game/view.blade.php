@extends('layout.app')

@section('body')
    <h1>{{ $game->title }}</h1>
    <div class="card w-100">
        <div class="card-header">
            Jeu réalisé dans le cadre de la <b>{{ $game->jam->title }}</b>.<br/>
            Dernière modification :
            @if($game->updated_at)
                {{ date('d/m/Y à H:i', strtotime($game->updated_at)) }}
            @else
                {{ date('d/m/Y à H:i', strtotime($game->created_at )) }}
            @endif
        </div>
        <div class="card-body">
            @if(auth()->id() == $game->user_id)
                <a href="{{ route('game-edit', [$game->id]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.game.edit') }}
                </a>
            @endif
            <a href="{{ route('jam', [$game->jam->url_title]) }}" class="btn btn-primary mr-1">
                {{ __('potat0s.jam.return') }}
            </a>
            <a href="{{ $game->url_itchio }}" target="_blank" class="btn btn-primary mr-1">
                {{ __('potat0s.game.play_it') }}
            </a>
        </div>
    </div>
    <div>
        <h2>{{ __('potat0s.game.description') }}</h2>
        <div>
            {!! $game->description !!}
        </div>
    </div>
    @if($notable)
        <div class="card w-100">
            <div class="card-header">
                <h2>{{ __('potat0s.game.add_note') }}</h2>
            </div>
            <div class="card-body">
                <x-form::simple-form class="text-left" :formUrl="route('game-note-post', [$game->id])">
                    <input type="hidden" name="submission_id" value="{{ $game->id }}"/>
                    @foreach($game->jam->jamCriteria as $criteria)
                        <x-form::form-line>
                            @php($note_index = $loop->index)
                            <label for="note_{{ $criteria->id }}">{{ $criteria->name }}</label>
                            @if(count($game['notes']) > 0 && isset($game['notes'][$note_index]->id))
                                <input type="hidden" name="notes[{{$note_index}}][id]" value="{{ $game['notes'][$note_index]['id'] }}"/>
                            @endif
                            <input type="hidden" name="notes[{{$note_index}}][criteria_id]" value="{{ $criteria->id }}"/>
                            <div>
                                @for($i = 1; $i <= 5; $i++)
                                    <span class="radiobox button">
                                        <input id="note_{{ $criteria->id }}_{{ $i }}" type="radio"
                                               name="notes[{{$note_index}}][note]" value="{{ $i }}" @if(count($game['notes']) > 0 && $game['notes'][$note_index]['note'] == $i) checked @endif />
                                        <label for="note_{{ $criteria->id }}_{{ $i }}">{{ $i }}</label>
                                    </span>
                                @endfor
                            </div>
                        </x-form::form-line>
                    @endforeach
                    <x-slot name="submitSlot">
                        {{ __('potat0s.game.send_or_update_note') }}
                    </x-slot>
                </x-form::simple-form>
            </div>
        </div>
    @endif
@endsection

@section('css')
    .radiobox {
        width: 20%;
        float: left;
        display: inline-block;
    }

    .radiobox input {
        width: 17px;
        height: 17px;
    }

    .radiobox label {
        width: auto;
        font-size: 20px;
        padding: 0 0 0 10px;
    }

    .radiobox.button label {
        background: #820386;
        text-align: center;
        width: 100%;
        padding: 0;
    }

    .radiobox.button label:hover {
        cursor: pointer;
        background: #78037c;
    }

    .radiobox.button label.active {
        background: #630366;
    }

    .radiobox.button input {
        display: none;
    }
    .radiobox input[type="radio"]:checked+label {
        background-color: #630366;
    }

    @media screen and (max-width: 992px) {
        .radiobox {
            width: 100%;
        }

        .radiobox.button {
            width: 20%;
        }

        .radiobox input {
            width: 15px;
            height: 15px;
        }

        .radiobox label {
            width: auto;
            font-size: 18px;
            padding: 0 0 0 5px;
        }
    }
@endsection
