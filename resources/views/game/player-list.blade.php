@extends('layout.app')

@section('body')
    <h1>{{ str_replace('{0}', $user->name, __('potat0s.page.game-list')) }}</h1>
    <div class="row">
        @foreach($user->submissions as $game)
            <div class="col showcase col-3 small-sc">
                <div class="sub-showcase">
                    <a href="{{ route('game', [$game->id]) }}">
                        <span class="small-sc">
                            {{ $game->title }}
                        </span>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
