@extends('layout.app')

@section('body')
    @if(isset($game))
        <x-b-modal id="deleteGameModal">
            <x-slot name="header_slot">
                {{ __('potat0s.game.delete_modal') }}
            </x-slot>
            <div class="text-center">
                {{ str_replace('{0}', $game->title, __('potat0s.game.delete_confirm')) }}
            </div>
            <x-slot name="footer_slot">
                <form method="post" action="{{ route('game-delete', [$game->id]) }}">
                    @csrf
                    <button class="btn btn-primary mr-1">{{ __('potat0s.game.delete') }}</button>
                </form>
                <a data-dismiss="modal" class="btn btn-primary mr-1">
                    {{ __('potat0s.game.no') }}
                </a>
            </x-slot>
        </x-b-modal>
        <h1>Editer {{ $game->title }}</h1>
    @else
        <h1>Publier mon jeu</h1>
    @endif
    <div class="card w-100">
        <div class="card-body">
            <a href="{{ route('jam', [$jam->url_title]) }}" class="btn btn-primary mr-1">
                {{ __('potat0s.jam.return') }}
            </a>
            @if(isset($game))
                <a href="{{ route('game', [$game->id]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.game.go_to_page') }}
                </a>
                <a onclick="deleteGame()" href="#" class="btn btn-primary mr-1">
                    {{ __('potat0s.game.delete') }}
                </a>
            @endif
        </div>
    </div>
    <x-form::simple-form
        formUrl="{{ !isset($game) ? route('game-publish-post') : route('game-edit-post', [$game->id]) }}">
        <input type="hidden" name="jam_id" value="{{ $jam->id }}">
        <x-form::form-line>
            <label for="gameTitle">
                {{ __('potat0s.form.game.title') }}
            </label>
            @error('title')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="gameTitle" type="text" name="title" value="{{ old('title', isset($game) ? $game->title : '') }}">
        </x-form::form-line>
        <x-form::form-line>
            <label for="gameUrl">
                {{ __('potat0s.form.game.url') }}
            </label>
            @error('url_itchio')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="gameUrl" type="url" name="url_itchio" value="{{ old('url_itchio', isset($game) ? $game->url_itchio : '') }}">
        </x-form::form-line>
        <x-form::form-line>
            <label for="vignettteUrl">
                {{ __('potat0s.form.game.vignette') }}
            </label>
            @error('vignette_url')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="vignettteUrl" type="url" name="vignette_url" value="{{ old('vignette_url', isset($game) ? $game->vignette_url : '') }}">
        </x-form::form-line>
        <x-form::form-line>
            <label for="gameDescription">
                {{ __('potat0s.form.game.description') }}
            </label>
            @error('description')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <textarea id="gameDescription" name="description">{{ old('description', isset($game) ? $game->description : '') }}</textarea>
        </x-form::form-line>
    </x-form::simple-form>
@endsection

@section('css')
    .note-editor {
    background: #1a202c;
    border: 1px solid #4a5568;
    border-radius: 5px;
    padding: 10px;
    text-align: initial;
    }

    .note-btn-group .note-btn {
    font-weight: 700;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    background: transparent;
    border: 0;
    color: #ffffff;
    padding: .2rem .5rem;
    margin-right: .2rem;
    border-radius: 3px;
    cursor: pointer;
    }

    .note-btn-group .note-btn:hover {
    background-color: rgba(255, 255, 255, 0.12);
    }

    .note-btn.disabled {
    background-color: grey;
    }

    .note-popover {
    background: rgb(37, 42, 53);
    }

    .editor-content {
    background: #524653;
    border-radius: 5px;
    box-shadow: inset 0 0 5px 0 black;
    }

    .editor-content .ProseMirror {
    min-height: 135px;
    padding: 10px;
    }

    .note-dropdown-menu {
    background: #252a35;
    }
@endsection

@push('js')
    <script src="{{ asset('summernote/summernote-bs4.min.js') }}"></script>
    <link href="{{ asset('summernote/summernote-bs4.min.css') }}" rel="stylesheet"/>
    <script>
        function deleteGame() {
            $('#deleteGameModal').modal({
                show: true
            });
        }

        $(document).ready(function () {
            $('#gameDescription').summernote({
                codeviewIframeFilter: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear', 'style', 'paragraph']],
                    ['misc', ['picture', 'link', 'undo', 'redo', 'codeview']]
                ]
            });
        });
    </script>
@endpush
