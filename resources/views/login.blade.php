@extends('layout.app')

@section('body')
    <h1>
        {{ __('potat0s.page.login') }}
    </h1>

@endsection

@section('css')
    .home .row {
        margin: 0;
    }

    @media screen and (max-width: 576px) {
        .home .bad-row .col-12 {
            padding: 0;
        }
    }
@endsection
