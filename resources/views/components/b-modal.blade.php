<div class="modal" tabindex="-1" id="{{$id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $header_slot }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ $slot }}
            </div>
            @if(!$hideFooter)
                <div class="modal-footer">
                    {{ $footer_slot }}
                </div>
            @endif
        </div>
    </div>
</div>
