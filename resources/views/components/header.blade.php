<div class="navbar navbar-dark bg-brown navbar-expand-lg">
    <a href="/" target="_self" class="navbar-brand">
        <img class="logo" alt="Website logo" src="{{ asset('assets/logo.png') }}"/>
    </a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#nav-collapse"
            aria-controls="nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="nav-collapse" class="navbar-collapse collapse">
        <ul class="navbar-nav nav-menu">
            <li class="nav-item">
                <a href="{{ url('calendar') }}" class="nav-link" target="_self">
                    {{ __('potat0s.menu.calendar') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ url('jam/list') }}" class="nav-link" target="_self">
                    {{ __('potat0s.menu.jams') }}
                </a>
            </li>
            @if(Auth::user() != null)
                <li class="nav-item">
                    <a href="https://discord.gg/Ew6A2WXsEY" class="nav-link" target="_self">
                        {{ __('potat0s.menu.discord') }}
                    </a>
                </li>
            @endif
        </ul>
    </div>
    <ul class="navbar-nav">
        <li class="nav-item dropdown nav-user">
            @if(Auth::user() != null)
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span>
                        &#x25BC;
                        <span class="username">
                            {{ Auth::user()->name }}
                        </span>
                        @if(Auth::user()->avatarUrl() == null)
                            <img src="{{ url('/assets/avatar_patate.png') }}" alt="Avatar"/>
                        @else
                            <img src="{{ Auth::user()->avatarUrl() }}" alt="Avatar"/>
                        @endif
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('games-player', [Auth::user()->name]) }}">
                        {{ __('potat0s.session.my_games') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('parameters') }}">
                        {{ __('potat0s.session.parameters') }}
                    </a>
                    <a class="dropdown-item" href="{{ url('logout') }}">
                        {{ __('potat0s.session.logout') }}
                    </a>
                </div>
            @else
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <span>
                        &#x25BC;
                        <img src="{{ asset('assets/avatar_anonyme.png') }}" alt="Avatar"/>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#loginModal">
                        {{ __('potat0s.session.login') }}
                    </a>
                    <a class="dropdown-item" href="{{ url('inscription') }}">
                        {{ __('potat0s.session.register') }}
                    </a>
                </div>
            @endif
        </li>
    </ul>
</div>
