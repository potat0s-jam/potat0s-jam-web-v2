<form method="POST" class="{{ $class }}" action="{{ $formUrl }}">
    @csrf
    {{ $slot }}
    <x-form::form-line>
        <button type="submit">
            {{ $submitSlot ?? __('potat0s.form.send') }}
        </button>
        {{ $postButtonSlot ?? null }}
    </x-form::form-line>
</form>
