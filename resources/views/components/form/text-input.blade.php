<x-form-line>
    @if(!$no_label)
        <label for="{{ $id  }}">
            {{ $label_slot }}
        </label>
    @endif
    @if($inline)
        <input id="{{ $id }}" type="text" name="{{ $id }}" value="{{ $slot }}">
    @else
        <textarea id="{{ $id }}" name="{{ $id }}">
            {{ $slot }}
        </textarea>
    @endif
</x-form-line>

@push('js')
    <link href="{{ url('summernote/summernote.css') }}" rel="stylesheet"/>
    <script src="{{ url('summernote/summernote.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#{{ $id }}').summernote();
        });
    </script>
@endpush
