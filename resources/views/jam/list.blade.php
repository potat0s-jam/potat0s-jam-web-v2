@extends('layout.app')

@section('body')
    <h1>Les Potat0s Jams</h1>
    @if(Auth::user() != null && Auth::user()->is_admin)
        <a href="{{ route('jam-create') }}" class="btn btn-primary">Organiser une nouvelle jam</a>
    @endif
    <div class="row">
        @foreach($jamList as $jam)
            <div class="col col-6 showcase big-sc">
                <div class="sub-showcase" style="--background:url('https://img.itch.zone/aW1hZ2UyL2phbS8xMjk5OS8yOTAzMTUwLnBuZw==/100x79%23/5gxeNg.png')">
                    <a href="{{ url('/jam', [$jam->url_title]) }}">
                        <span class="big-sc">
                            {{ $jam->title }}
                        </span>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('css')

@endsection
