@extends('layout.app')

@section('body')
    <h1>{{ __('potat0s.jam.leaderboard_of') }} {{ $jam->title }}</h1>
    <div>
        <div class="card w-100">
            <div class="card-body">
                <a href="{{ route('jam', [$jam->url_title]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.jam.return') }}
                </a>
                <div class="text-left">
                    <label for="criteria">
                        {{ __('potat0s.jam.criterias') }}
                    </label>
                    <select id="criteria" onchange="changeCriteria(this.value)">
                        <option value="-1,{{ __('potat0s.jam.default_criteria') }}">
                            {{ __('potat0s.jam.default_criteria') }}
                        </option>
                        @foreach($jam->jamCriteria as $criteria)
                            <option value="{{$criteria->id}},{{$criteria->name}}">
                                {{ $criteria->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div>
            <table class="table table-striped table-dark table-hover">
                <thead class="thead-dark">
                <tr>
                    <th>
                        Rang
                    </th>
                    <th>
                        Participation
                    </th>
                    <th id="selectedCriteria">
                        {{ __('potat0s.jam.default_criteria') }}
                    </th>
                </tr>
                </thead>
                <tbody id="leaderboard">
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('js')
    <script>
        let leaderboard = @json($leaderboard);
        let sortedLeaderboard;
        let selectedCriteria = -1;

        sortLeaderboard();
        showLeaderboard();

        function changeCriteria (criteria) {
            let criteriaData = criteria.split(',');
            document.getElementById('selectedCriteria').textContent = criteriaData[1];

            selectedCriteria = parseInt(criteriaData[0]);

            sortLeaderboard();
            showLeaderboard();
        }

        function showLeaderboard(){
            let html = document.getElementById('leaderboard');
            html.innerHTML = '';
            if(sortedLeaderboard.length > 0){
                sortedLeaderboard.forEach((game, index) => {
                    html.innerHTML += `
                <tr>
                    <td>
                        ${ index + 1 }
                    </td>
                    <td>
                        <a href="${game.route}" target="_blank">
                            ${ game.title }
                        </a>
                    </td>
                    <td>
                        ${ game.leaderboard[selectedCriteria === -1 ? "overall" : selectedCriteria].note_total === -1 ? 'Non noté' : game.leaderboard[selectedCriteria === -1 ? "overall" : selectedCriteria].note_total}
                    </td>
                </tr>`;
                });
            }
        }

        function sortLeaderboard(){
            sortedLeaderboard = leaderboard.sort(function(gameA, gameB) {
                let gameAScore = (selectedCriteria === -1 ? gameA.leaderboard['overall'].note_total : gameA.leaderboard[`${selectedCriteria}`].note_total);
                let gameBScore = (selectedCriteria === -1 ? gameB.leaderboard['overall'].note_total : gameB.leaderboard[`${selectedCriteria}`].note_total);
                return parseFloat(gameAScore) < parseFloat(gameBScore) ? 1 : -1;
            });
        }
    </script>
@endpush
