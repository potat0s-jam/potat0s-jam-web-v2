@extends('layout.app')

@section('body')
    @if(isset($jam))
        <h1>Editer {{ $jam->title }}</h1>
        <div class="card w-100">
            <div class="card-body">
                <a href="{{ route('jam', [$jam->url_title]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.jam.go_to') }}
                </a>
            </div>
        </div>
    @else
        <h1>Créer une Jam</h1>
    @endif
    <x-form::simple-form :formUrl="isset($jam) ? route('jam-edit-post', [$jam->id]) : route('jam-post')">
        <div id="vueForm">
            <x-form::form-line>
                <label for="title">
                    {{ __('potat0s.form.jam.title') }}
                </label>
                @error('title')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <input id="title" type="text" name="title" value="{{ old('title', isset($jam) ? $jam->title : '') }}"/>
            </x-form::form-line>
            <x-form::form-line>
                <label>
                    {{ __('potat0s.form.jam.type') }}
                </label>
                @error('ranked')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <div>
                        <span class="radio">
                            <input id="ranked" type="radio" v-model="ranked"
                                   name="ranked" value="1" @if(old('ranked', isset($jam) && $jam->is_ranked)) checked @endif />
                            <label for="ranked">{{ __('potat0s.form.ranked') }}</label>
                        </span>
                    <span class="radio">
                            <input id="not_ranked" type="radio" v-model="ranked"
                                   name="ranked" value="0" @if(!old('ranked', isset($jam) && $jam->is_ranked)) checked @endif />
                            <label for="not_ranked">{{ __('potat0s.form.not_ranked') }}</label>
                        </span>
                </div>
            </x-form::form-line>
            <x-form::form-line>
                <label for="theme">
                    {{ __('potat0s.form.jam.theme') }}
                </label>
                @error('theme')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <textarea id="theme" name="theme">{{ old('theme', isset($jam) ? $jam->theme : '') }}</textarea>
            </x-form::form-line>
            <x-form::form-line>
                <label for="dates">
                    {{ __('potat0s.form.jam.dates') }}
                </label>
                <select id="dates" v-model="listInput">
                    <option value="start">{{ __('potat0s.form.dates.start') }}</option>
                    <option value="end" v-if="startDate !== null">{{ __('potat0s.form.dates.end') }}</option>
                    <option value="end_vote" v-if="endDate !== null && ranked === '1'">
                        {{ __('potat0s.form.dates.end_vote') }}
                    </option>
                </select>
            </x-form::form-line>
            <x-form::form-line :class="'row time'">
                <div class="col col-4">
                    <label for="start_time">
                        {{ __('potat0s.form.jam.start_time') }}
                    </label>
                    <input type="time" id="start_time" v-model="startTime" @change="updateTime('start')">
                </div>
                <div class="col col-4">
                    <label for="end_time">
                        {{ __('potat0s.form.jam.end_time') }}
                    </label>
                    <input type="time" id="end_time" v-model="endTime" @change="updateTime('end')">
                </div>
                <div class="col col-4" v-if="ranked === '1'">
                    <label for="end_vote">
                        {{ __('potat0s.form.jam.end_vote') }}
                    </label>
                    <input type="time" id="end_vote" v-model="endVoteTime" @change="updateTime('endVote')">
                </div>
            </x-form::form-line>
            <x-form::form-line :class="'no-overflow'">
                @error('start_jam')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                @error('end_jam')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                @error('end_vote')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <calendarinput ref="nextJamDates" v-on:calendar-click="AddDate"></calendarinput>
                <input type="hidden" name="start_jam" v-model="startJamFormat"/>
                <input type="hidden" name="end_jam" v-model="endJamFormat"/>
                <input type="hidden" name="end_vote" v-if="ranked == '1'" v-model="endVoteFormat"/>
                <div class="border-primary" v-if="startJamFormat !== '' || endJamFormat !== '' || endVoteFormat !== ''">
                    Jam du <b>@{{ startJamFormat }}</b> au <b>@{{ endJamFormat }}</b>
                    <template v-if="endVoteFormat !== ''">
                        - Fin du vote <b>@{{ endVoteFormat }}</b>
                    </template>
                </div>
            </x-form::form-line>
            <x-form::form-line>
                <label for="jamDescription">
                    {{ __('potat0s.form.jam.description') }}
                </label>
                @error('description')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <textarea id="jamDescription"
                          name="description">{{ old('description', isset($jam) ? $jam->description : '') }}</textarea>
            </x-form::form-line>
            <x-form::form-line>
                <label>
                    {{ __('potat0s.form.jam.state') }}
                </label>
                @error('published')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <div>
                    <span class="radio">
                        <input id="published" type="radio"
                               name="published" value="1"
                               @if(old('published', isset($jam) ? $jam->is_published : false)) checked @endif/>
                        <label for="published">{{ __('potat0s.form.published') }}</label>
                    </span>
                    <span class="radio">
                        <input id="not_published" type="radio"
                               name="published" value="0"
                               @if(old('published', isset($jam) ? !$jam->is_published : true)) checked @endif/>
                        <label for="not_published">{{ __('potat0s.form.not_published') }}</label>
                    </span>
                </div>
            </x-form::form-line>
            <x-form::form-line>
                <label>
                    {{ __('potat0s.form.jam.criterias') }}
                </label>
                @error('criterias')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
                <div id="criterias">
                    <div class="container row criteria" v-for="(criteria, index) in arrayInput" v-bind:key="index">
                        <div class="col col-10">
                            <input type="text" v-model="arrayInput[index].name"/>
                            <input type="hidden" name="criterias[]"
                                   :value="JSON.stringify({'id': criteria.id, 'name': criteria.name})"/>
                        </div>
                        <button v-on:click="removeCriteria(index)" type="button" class="col col-2">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
                <div class="container row">
                    <div class="col col-10"></div>
                    <button v-on:click="addCriteria()" type="button" class="col col-2">
                        <i class="fas fa-plus"></i>
                    </button>
                </div>
            </x-form::form-line>
        </div>
    </x-form::simple-form>
@endsection

@section('css')
    .radio {
    width: 50%;
    display: inline-block;
    float: left;
    font-size: 20px;
    }

    .radio input{
    width: 17px;
    height: 17px;
    }

    .time {
    margin: 25px -15px 0;
    }

    .note-editor {
    background: #1a202c;
    border: 1px solid #4a5568;
    border-radius: 5px;
    padding: 10px;
    text-align: initial;
    }

    .note-btn-group .note-btn {
    font-weight: 700;
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    background: transparent;
    border: 0;
    color: #ffffff;
    padding: .2rem .5rem;
    margin-right: .2rem;
    border-radius: 3px;
    cursor: pointer;
    }

    .note-btn-group .note-btn:hover {
    background-color: rgba(255, 255, 255, 0.12);
    }

    .note-btn.disabled {
    background-color: grey;
    }

    .editor-content {
    background: #524653;
    border-radius: 5px;
    box-shadow: inset 0 0 5px 0 black;
    }

    .editor-content .ProseMirror {
    min-height: 135px;
    padding: 10px;
    }

    .note-dropdown-menu {
    background: #252a35;
    }

    .note-popover {
    background: rgb(37, 42, 53);
    }

    .criteria {
    margin-bottom: 5px;
    }
@endsection

@push('js')
    @if(env('APP_ENV') == 'production')
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    @else
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    @endif
    <script src="{{ asset('moment/moment.js') }}"></script>
    <script src='https://unpkg.com/v-calendar@1.0.8'></script>
    <script src="{{ asset('js/calendarinput.js') }}"></script>
    <link href="{{ asset('css/calendarinput.css') }}" rel="stylesheet"/>

    <link href="{{ asset('font/all.min.css') }}" rel="stylesheet"/>

    <script>
        let dateFormat = 'YYYY-MM-DD hh:mm:ss';
        Vue.component('calendarinput', CalendarInput);
        let app = new Vue({
            el: '#vueForm',
            data() {
                return {
                    listInput: 'start',
                    startDate: null,
                    endDate: null,
                    endVoteDate: null,
                    startTime: '00:00',
                    endTime: '00:00',
                    endVoteTime: '00:00',
                    ranked: '{{ old('ranked', isset($jam) ? $jam->is_ranked : '1') }}',
                    startJamFormat: '',
                    endJamFormat: '',
                    endVoteFormat: '',
                    arrayInput: @if(isset($jam)) @json($jam->jamCriteria) @else [] @endif
                }
            },
            mounted() {
                let fullStartDate = '{{ old('start_jam', isset($jam) ? $jam->start_jam : '') }}';
                let fullEndDate = '{{ old('end_jam', isset($jam) ? $jam->end_jam : '') }}';
                let fullEndVoteDate = '{{ old('end_vote', isset($jam) ? $jam->end_vote : '') }}';

                if (fullStartDate !== '') {
                    this.startDate = moment.utc(fullStartDate, dateFormat);
                    this.startTime = this.startDate.format('HH:mm');
                    this.startJamFormat = fullStartDate;
                }

                if (fullEndDate !== '') {
                    this.endDate = moment.utc(fullEndDate, dateFormat);
                    this.endTime = this.endDate.format('HH:mm');
                    this.endJamFormat = fullEndDate;
                }

                if (this.startDate != null && this.endDate != null) {
                    this.$refs.nextJamDates.AddRangeEvent(this.startDate.toDate(), this.endDate.toDate(), 'Déroulement de la jam', 'nextjam', 'blue')
                }

                if (fullEndVoteDate !== '') {
                    this.endVoteDate = moment.utc(fullEndVoteDate, dateFormat);
                    this.$refs.nextJamDates.AddRangeEvent(this.endDate.toDate(), this.endVoteDate.toDate(), 'Votes de la jam', 'votejam', 'red');
                    this.endVoteTime = this.endVoteDate.format('HH:mm');
                    this.endVoteFormat = fullEndVoteDate;
                }
            },
            methods: {
                updateTime(type) {
                    switch (type) {
                        case 'start':
                            if (this.startDate != null) {
                                this.startJamFormat = `${this.startDate.format('YYYY-MM-DD')} ${this.startTime}`
                            }
                            break;
                        case 'end':
                            if (this.endDate != null) {
                                this.endJamFormat = `${this.endDate.format('YYYY-MM-DD')} ${this.endTime}`
                            }
                            break;
                        case 'endVote':
                            if (this.endVoteDate != null) {
                                this.endVoteFormat = `${this.endVoteDate.format('YYYY-MM-DD')} ${this.endVoteTime}`
                            }
                            break;
                    }
                },
                AddDate(day) {
                    let dayDate = moment(day.date);
                    let typeDateSelect = this.listInput
                    if (typeDateSelect === 'start' && dayDate.toDate() >= this.$refs.nextJamDates.dayDate) {
                        this.startDate = dayDate
                        this.startJamFormat = `${dayDate.format('YYYY-MM-DD')} ${this.startTime}`
                        if (this.endDate !== null && dayDate.toDate() <= this.endDate.toDate()) {
                            this.$refs.nextJamDates.AddRangeEvent(this.startDate.toDate(), this.endDate.toDate(), 'Déroulement de la jam', 'nextjam', 'blue')
                        } else if (this.endDate === null) {
                            this.listInput = 'end'
                            this.$refs.nextJamDates.AddSimpleHighlightEvent(this.startDate.toDate(), 'Déroulement de la jam', 'nextjam', 'blue')
                        }
                    } else if (typeDateSelect === 'end' && dayDate.toDate() >= this.startDate.toDate() && (this.endVoteDate === null || (dayDate.toDate() <= this.endVoteDate.toDate()))) {
                        this.endDate = dayDate
                        this.endJamFormat = `${dayDate.format('YYYY-MM-DD')} ${this.endTime}`
                        this.$refs.nextJamDates.AddRangeEvent(this.startDate.toDate(), this.endDate.toDate(), 'Déroulement de la jam', 'nextjam', 'blue')
                        if (this.endVoteDate !== null) {
                            this.$refs.nextJamDates.AddRangeEvent(this.endDate.toDate(), this.endVoteDate.toDate(), 'Votes de la jam', 'votejam', 'red')
                        } else if (this.ranked === '1') {
                            this.listInput = 'end_vote'
                        }
                    } else if (typeDateSelect === 'end_vote' && this.ranked === '1' && dayDate.toDate() >= this.endDate.toDate()) {
                        this.endVoteDate = dayDate
                        this.endVoteFormat = `${dayDate.format('YYYY-MM-DD')} ${this.endVoteTime}`
                        this.$refs.nextJamDates.AddRangeEvent(this.endDate.toDate(), this.endVoteDate.toDate(), 'Votes de la jam', 'votejam', 'red')
                    }
                },
                removeCriteria(index) {
                    Vue.delete(this.arrayInput, index)
                },
                addCriteria() {
                    Vue.set(this.arrayInput, this.arrayInput.length, {name: ''})
                }
            }
        })
    </script>

    <script src="{{ asset('summernote/summernote-bs4.min.js') }}"></script>
    <link href="{{ asset('summernote/summernote-bs4.min.css') }}" rel="stylesheet"/>
    <script>
        function deleteGame() {
            $('#deleteGameModal').modal({
                show: true
            });
        }

        var ThemeButton = function (context) {
            var ui = $.summernote.ui;

            // create button
            var button = ui.button({
                contents: '<i class="fas fa-trophy"/> Thème',
                tooltip: 'Thème de la jam',
                click: function () {
                    // invoke insertText method with 'hello' on editor module.
                    context.invoke('editor.insertText', '[theme]');
                }
            });

            return button.render();   // return button as jquery object
        }

        $(document).ready(function () {
            $('#theme').summernote({
                codeviewIframeFilter: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear', 'style', 'paragraph']],
                    ['misc', ['picture', 'link', 'undo', 'redo', 'codeview']]
                ]
            });

            $('#jamDescription').summernote({
                codeviewIframeFilter: true,
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear', 'style', 'paragraph']],
                    ['misc', ['theme', 'picture', 'link', 'undo', 'redo', 'codeview']]
                ],
                buttons: {
                    theme: ThemeButton
                }
            });
        });
    </script>
@endpush
