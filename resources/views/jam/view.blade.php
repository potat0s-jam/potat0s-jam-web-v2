@extends('layout.app')

@section('body')
    <h1>{{ $jam->title }}</h1>
    <div class="jam-info">
        <div class="jam-type d-inline-block align-middle">
            <span>
                {{ __($jam->is_ranked ? 'potat0s.form.ranked' : 'potat0s.form.not_ranked') }}
                - {{ __($jam->is_published ? 'potat0s.form.published' : 'potat0s.form.not_published') }}
            </span>
        </div>
        <div class="float-right d-inline-block">
            <div class="text-center d-inline-block">
                {{ $jam->nb_users }}
                <div>Inscrits</div>
            </div>
            <div class="text-center d-inline-block ml-3">
                {{ $jam->nb_submissions }}
                <div>Jeux</div>
            </div>
            <div class="text-center d-inline-block ml-3">
                {{ $jam->nb_notes }}
                <div>Notes</div>
            </div>
        </div>
    </div>
    <div class="card w-100">
        <div class="card-header">
            Déroulement de la jam du <b>{{ date('d/m/Y à H:i', strtotime($jam->start_jam)) }}</b>
            au <b>{{ date('d/m/Y à H:i', strtotime($jam->end_jam)) }}</b>
        </div>
        <div class="card-body">
            @if(Auth::user() != null && Auth::user()->is_admin)
                <a href="{{ route('jam-edit', [$jam->url_title]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.jam.edit') }}
                </a>
            @endif
            @if($jam->is_ranked && time() > strtotime($jam->end_vote))
                <a href="{{ route('jam-leaderboard', [$jam->url_title]) }}" class="btn btn-primary mr-1">
                    {{ __('potat0s.jam.leaderboard') }}
                </a>
            @endif
            @if(Auth::user() == null)
                @if($isJoinable)
                    <div class="alert alert-info">
                        {{ __('potat0s.jam.login_before_participate') }}
                    </div>
                @endif
            @else
                @if($jam->has_join)
                    @if($isJoinable)
                        <a href="{{ route('jam-leave', $jam->url_title) }}" class="btn btn-primary mr-1">
                            {{ __('potat0s.jam.leave') }}
                        </a>
                    @endif
                    @if($isPublishable)
                        <a class="btn btn-primary ml-1" href="{{ route('game-publish', [$jam->url_title]) }}">
                            {{ __('potat0s.jam.publish') }}
                        </a>
                    @endif
                @else
                    @if($isJoinable)
                        <a href="{{ route('jam-join', $jam->url_title) }}" class="btn btn-primary mr-1">
                            {{ __('potat0s.jam.join')}}
                        </a>
                    @endif
                @endif
            @endif
        </div>
    </div>
    <div>
        <h2>Description de la jam</h2>
        {!! str_replace('[theme]', strtotime(now()) > strtotime($jam->start_jam) ? $jam->theme : __('potat0s.jam.theme_soon') , $jam->description) !!}
    </div>
    @if(count($jam->submissions) > 0)
        <div class="container">
            <h2>Jeux de la jam</h2>
            <div class="row">
                @foreach($jam->submissions as $game)
                    <div class="col showcase col-3 small-user-sc">
                        <div style="--game-vignette: url({{ $game->vignette_url }})" class="sub-showcase small-user-sub-sc">
                            <a href="{{ route('game', [$game->id]) }}">
                                <span class="small-sc">
                                    {{ $game->title }}
                                </span>
                            </a>
                        </div>
                        <div class="showcase-user">
                            <a href="{{ route('games-player', [$game->user->name]) }}">
                                {{ $game->user->name }}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection

@section('css')
    .jam-info {
    position: relative;
    height: 50px;
    border-bottom: 1px solid #382e2d;
    margin: 0 0 10px 0;
    }

    .jam-type {
    line-height: 50px;
    }

    .container h2 {
    margin: 0 -15px;
    }

    .alert {
    margin-bottom: 0;
    }
@endsection
