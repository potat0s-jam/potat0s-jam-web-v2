@extends('layout.app')

@section('body')
    <h1>{{ __('potat0s.page.calendar') }}</h1>
    <div id="vueCalendar">
        <calendarinput ref="nextJamDates"></calendarinput>
    </div>
@endsection

@section('css')
@endsection

@push('js')
    @if(env('APP_ENV') == 'production')
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>
    @else
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    @endif
    <script src='https://unpkg.com/v-calendar@1.0.8'></script>
    <script src="{{ asset('js/calendarinput.js') }}"></script>
    <link href="{{ asset('css/calendarinput.css') }}" rel="stylesheet"/>

    <script>
        let dateFormat = 'YYYY-MM-DD hh:mm:ss';
        Vue.component('calendarinput', CalendarInput);
        let app = new Vue({
            el: '#vueCalendar',
            data () {
                return {
                    jamList: @json(count($jamList) > 0 ? $jamList : [])
                }
            },
            mounted() {
                if (this.jamList.length > 0) {
                    this.jamList.forEach((jam) => {
                        this.$refs.nextJamDates.AddRangeEvent(new Date(jam.start_jam), new Date(jam.end_jam), jam.title, jam.url_title, 'blue')
                        if (jam.end_vote != null) {
                            this.$refs.nextJamDates.AddRangeEvent(new Date(jam.end_jam), new Date(jam.end_vote), `Votes de la ${jam.title}`, `${jam.url_title}_votejam`, 'red')
                        }
                    });
                }

            }
        });
    </script>
@endpush

