<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <link rel="icon" href="{{ asset('assets/avatar_patate.png') }}">
    <title>Potat0s Game Jam @if(isset($title))- {{ $title }}@endif</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/form.css') }}">
    <style>
        @yield('css')
    </style>
</head>
<body>
    <x-header></x-header>
    <x-b-modal id="loginModal" :hideFooter="true">
        <x-slot name="header_slot">
            {{ __('potat0s.page.login') }}
        </x-slot>
        <x-form::simple-form :formUrl="url('/login')">
            @if(session('error-modal'))
                <div class="modal-alert alert alert-danger" id="header-alert">
                    <button type="button" class="close" aria-label="Close" onclick="$('#header-alert').toggle()">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ __(session('error-modal')) }}
                </div>
            @endif
            <x-form::form-line>
                <label for="email">{{ __('potat0s.form.login.email') }}</label>
                <input id="email" type="email" name="email"/>
            </x-form::form-line>
            <x-form::form-line>
                <label for="password">{{ __('potat0s.form.login.password') }}</label>
                <input id="password" type="password" name="password" />
                {{ __('potat0s.form.forgot_password') }} <a href="{{ route('reset-password') }}">{{__('potat0s.form.click_here')}}</a>
            </x-form::form-line>
            <x-form::form-line>
                <input id="remember" type="checkbox" name="remember" />
                <label for="remember" class="inline">{{ __('potat0s.form.login.remember') }}</label>
            </x-form::form-line>
            <x-slot name="submitSlot">{{ __('potat0s.form.login.button')}}</x-slot>
            <x-slot name="postButtonSlot">
                <div>
                    {{ __('potat0s.form.need_account') }}
                    <a href="{{ url('/inscription') }}">{{ __('potat0s.form.click_register') }}</a>
                </div>
            </x-slot>
        </x-form::simple-form>
    </x-b-modal>
    <div class="container body-site">
        @if(session('success'))
            <div class="header alert alert-success" id="header-alert">
                <button type="button" class="close" aria-label="Close" onclick="$('#header-alert').toggle()">
                    <span aria-hidden="true">×</span>
                </button>
                {{ __(session('success')) }}
            </div>
        @endif
        @if(session('error'))
            <div class="header alert alert-danger" id="header-alert-error">
                <button type="button" class="close" aria-label="Close" onclick="$('#header-alert-error').toggle()">
                    <span aria-hidden="true">×</span>
                </button>
                {{ __(session('error')) }}
            </div>
        @endif
        <div class="row">
            <div class="col col-12 col-lg-10 body-content">
                <div class="page-block">
                    @yield('body')
                </div>
            </div>
            <div class="col col-12 col-lg-2 right-col">
                <div class="container">
                    <div class="row">
                        @php
                            $jam = \App\Models\Jam::orderByDesc('start_jam')->first();
                            $class = 'next-edition';
                            $nextEditionText = '';
                            if($jam != null){
                                if((($jam->is_ranked) && time() > strtotime($jam->end_vote)) || (!$jam->is_ranked && time() > strtotime($jam->end_jam))){
                                    $nextEditionText = __('potat0s.jam.no_jam');
                                } elseif ($jam->is_ranked && time() > strtotime($jam->end_jam) && time() < strtotime($jam->end_vote)){
                                    $nextEditionText = '<a href="'.route('jam', [$jam->url_title]).'">'.__('potat0s.jam.actual_vote').'</a>';
                                } elseif (time() >= strtotime($jam->start_jam) && time() < strtotime($jam->end_jam)){
                                    $nextEditionText = '<a href="'.route('jam', [$jam->url_title]).'">'.__('potat0s.jam.on_going').'</a>';
                                } else {
                                    $nextEditionText = '<a href="'.route('jam', [$jam->url_title]).'">'.__('potat0s.jam.next_edition').'</a>';
                                    $remaining = strtotime($jam->start_jam) - time();
                                    $days_remaining = floor($remaining / 86400);
                                    $hours_remaining = floor($remaining / 3600);
                                    $minutes_remaining = floor($remaining / 60);
                                    $seconds_remaining = $remaining;
                                }
                            }
                        @endphp
                        <div class="col col-3 col-lg-12 {{ $class }}">
                            {!! $nextEditionText !!}
                        </div>
                        @if(isset($remaining))
                            <div class="col col-3 col-lg-7 next-date">
                                <span>
                                    {{ \Carbon\Carbon::parse($jam->start_jam)->translatedFormat('l d F') }}
                                </span>
                            </div>
                            <div class="col col-6 col-lg-5 next-time">
                                <span>
                                    @php
                                        if($days_remaining > 0){
                                            echo '<span>'. $days_remaining .'</span>';
                                            echo __('potat0s.time.day');
                                        } elseif($hours_remaining > 0){
                                            echo '<span>'. $hours_remaining .'</span>';
                                            echo __('potat0s.time.hour');
                                        } elseif($minutes_remaining > 0){
                                            echo '<span>'. $minutes_remaining .'</span>';
                                            echo __('potat0s.time.minute');
                                        } else {
                                            echo '<span>'. $seconds_remaining .'</span>';
                                            echo __('potat0s.time.second');
                                        }
                                    @endphp
                                </span>
                            </div>
                        @endif
                        <div class="white-title d-none d-lg-block">
                            {{ __('potat0s.menu.actually_live') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="{{ url('bootstrap/js/bootstrap.min.js') }}"></script>
@if(session('error-modal'))
    <script>
        $(function() {
            $('#loginModal').modal({
                show: true
            });
        });
    </script>
@endif
@stack('js')
</html>
