@extends('layout.app')

@section('body')
    <h1>Demande de changement du mot de passe</h1>
    <x-form::simple-form formUrl="{{ route('reset-password-validate', [$token]) }}">
        <x-form::form-line>
            <label for="email_reset">
                {{ __('potat0s.form.email') }}
            </label>
            @error('email')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input disabled id="email_reset" type="email" value="{{ old('email', $email) }}"/>
            <input type="hidden" name="email" value="{{ old('email', $email) }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label for="password_reset">
                {{ __('potat0s.form.password') }}
            </label>
            @error('password')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="password_reset" type="password" name="password" value="{{ old('password') }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label for="confirm_password_reset">
                {{ __('potat0s.form.password_confirm') }}
            </label>
            @error('password_confirmation')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="confirm_password_reset" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}"/>
        </x-form::form-line>
        <input type="hidden" name="token" value="{{ $token }}">
        <x-form::form-line :class="'admin'">
            <label for="admin">{{ __('potat0s.form.admin') }}</label>
            <input id="admin" type="text" name="admin"/>
        </x-form::form-line>

    </x-form::simple-form>
@endsection

@section('css')
    .admin{
        opacity: 0;
        position: absolute;
        top: 0;
        left: 0;
        height: 0;
        width: 0;
        z-index: -1;
    }
@endsection
