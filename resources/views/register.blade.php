@extends('layout.app')

@section('body')
    <h1>Inscription</h1>
    <x-form::simple-form :formUrl="route('register-post')">
        <x-form::form-line>
            <label for="email_register">
                {{ __('potat0s.form.email') }}
            </label>
            @error('email')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="email_register" type="email" name="email" value="{{ old('email') }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label for="name">
                {{ __('potat0s.form.username') }}
            </label>
            @error('name')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="name" type="text" name="name" value="{{ old('name') }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label for="password_register">
                {{ __('potat0s.form.password') }}
            </label>
            @error('password')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="password_register" type="password" name="password" value="{{ old('password') }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label for="confirm_password_register">
                {{ __('potat0s.form.password_confirm') }}
            </label>
            @error('password_confirmation')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="confirm_password_register" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}"/>
        </x-form::form-line>
        <x-form::form-line>
            <label>
                {{ __('potat0s.form.avatar') }}
            </label>
            @error('avatar')
                <div class="text-danger">
                    {{ $message }}
                </div>
            @enderror
            <div>
                <span class="radio">
                    <input id="avatar_no" type="radio"
                           name="avatar" value="no" @if(old('avatar', 'no') == 'no') checked @endif />
                    <label for="avatar_no">{{ __('potat0s.form.avatar_no') }}</label>
                </span>
                <span class="radio">
                    <input id="avatar_discord" type="radio"
                           name="avatar" value="discord" @if(old('avatar') == 'discord') checked @endif />
                    <label for="avatar_discord">{{ __('potat0s.form.avatar_discord') }}</label>
                </span>
                <span class="radio">
                    <input id="avatar_gravatar" type="radio"
                           name="avatar" value="gravatar" @if(old('avatar') == 'gravatar') checked @endif />
                    <label for="avatar_gravatar">{{ __('potat0s.form.avatar_gravatar') }}</label>
                </span>
            </div>
        </x-form::form-line>
        <x-form::form-line>
            <span class="checkbox">
                <input id="newsletter" type="checkbox" name="want_newsletter" value="1" @if(old('newsletter')) checked @endif />
                <label for="newsletter">{{ __('potat0s.form.newsletter') }}</label>
            </span>
        </x-form::form-line>
        <x-form::form-line :class="'admin'">
            <label for="admin">{{ __('potat0s.form.admin') }}</label>
            <input id="admin" type="text" name="admin"/>
        </x-form::form-line>

        <x-slot name="submitSlot">{{ __('potat0s.form.register.button') }}</x-slot>
    </x-form::simple-form>
@endsection

@section('css')
    .radio {
    width: 33%;
    display: inline-block;
    float: left;
    font-size: 20px;
    }

    .radio input{
    width: 17px;
    height: 17px;
    }

    .admin{
    opacity: 0;
    position: absolute;
    top: 0;
    left: 0;
    height: 0;
    width: 0;
    z-index: -1;
    }
@endsection
