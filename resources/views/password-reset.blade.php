@extends('layout.app')

@section('body')
    <h1>Demande de changement du mot de passe</h1>
    <x-form::simple-form formUrl="{{ route('reset-password-email') }}">
        <x-form::form-line>
            <label for="email_reset">
                {{ __('potat0s.form.email') }}
            </label>
            @error('email')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input id="email_reset" type="email" name="email" value="{{ old('email') }}"/>
        </x-form::form-line>
        <x-form::form-line :class="'admin'">
            <label for="admin">{{ __('potat0s.form.admin') }}</label>
            <input id="admin" type="text" name="admin"/>
        </x-form::form-line>

    </x-form::simple-form>
@endsection

@section('css')
    .admin{
        opacity: 0;
        position: absolute;
        top: 0;
        left: 0;
        height: 0;
        width: 0;
        z-index: -1;
    }
@endsection
