<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{ __('potat0s.email.confirmation') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        .background {
            padding: 0;
            margin: 0;
            background: #311f39;
            font-family: sans-serif;
            color: white;
            font-size: 17px;
        }

        .logo, .footer {
            background: #382e2d;
            padding: 10px;
        }

        .footer {
            background: #73635a;
            position: absolute;
        }

        .content {
            background: #ffffff47;
            width: 90%;
            padding: 10px;
            margin: 25px auto;
            text-align: justify;
        }

        a {
            color: #33b8eb;
        }
    </style>
</head>
<body class="background">
<div class="logo">
    <a href="{{ route('home') }}" target="_blank" rel="nofollow noreferrer">
        <img src="{{ asset('assets/logo.png') }}" width="200" height="61" title="Logo de la Potat0s Game Jam" alt="Logo de la Potat0s Game Jam"/>
    </a>
</div>
<div class="content">
    Bienvenue {{ $username }},<br/>
    <br/>
    Afin de terminer votre inscription, merci de confirmer votre adresse email en cliquant sur le lien suivant :
    <a href="{{ route('confirm-email', [$emailToken]) }}">
        Terminer mon inscription
    </a>
</div>
<div class="footer">
    Si le clique du lien ne fonctionne pas, veuillez copier/coller le lien suivant
    : <b>{{ route('confirm-email', [$emailToken]) }}</b>
</div>
</body>
</html>
