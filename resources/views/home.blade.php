@extends('layout.app')

@section('body')
    <div class="row">
        <div class="col col-12">
            <h2>
                {{ __('potat0s.page.game_to_try') }}
            </h2>
            <div class="row bad-row">
                <div class="alert alert-info">
                    La liste des jeux n'est pas encore disponible. <br/>
                    Cette feature sera implémentée prochainement
                </div>
            </div>
        </div>
        <div class="col col-12">
            <h2>
                {{ __('potat0s.page.news') }}
            </h2>
            <div class="row bad-row">
                <div class="alert alert-info">
                    Les actus ne sont pas encore disponible.  <br/>
                    Cette feature sera implémentée prochainement
                </div>
            </div>
        </div>
        <div class="col col-12">
            <h2>
                {{ __('potat0s.page.last_topics') }}
            </h2>
            <div class="row bad-row">
                <div class="alert alert-info">
                    Le forum n'est pas encore disponible.  <br/>
                    Cette feature sera implémentée prochainement
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    .home .row {
    margin: 0;
    }

    @media screen and (max-width: 576px) {
    .home .bad-row .col-12 {
    padding: 0;
    }
    }
@endsection
