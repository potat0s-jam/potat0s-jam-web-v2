@extends('layout.app')

@section('body')
    <h1>{{ __('potat0s.page.account') }}</h1>
    <h2>{{ __('potat0s.page.parameters') }}</h2>
    <x-form::simple-form formUrl="{{ route('parameters-post') }}">
        <x-form::form-line>
            <label for="parameter_email">
                {{ __('potat0s.form.email') }}
            </label>
            <input type="email" id="parameter_email" name="email" value="{{ $user->email }}" disabled>
        </x-form::form-line>
        <x-form::form-line>
            <label for="name">
                {{ __('potat0s.form.username') }}
            </label>
            @error('name')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input type="text" id="name" name="name" value="{{ old('name', $user->name) }}">
        </x-form::form-line>
        <x-form::form-line>
            <label>
                {{ __('potat0s.form.avatar') }}
            </label>
            @error('avatar')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <div>
                <span class="radio">
                    <input id="avatar_no" type="radio" onclick="updateDiscord(false)"
                           name="avatar" value="no" @if(old('avatar', $user->avatar) == 'no') checked @endif />
                    <label for="avatar_no">{{ __('potat0s.form.avatar_no') }}</label>
                </span>
                <span class="radio">
                    <input id="avatar_discord" type="radio" onclick="updateDiscord({{$user->avatar == 'discord'}})"
                           name="avatar" value="discord" @if(old('avatar', $user->avatar) == 'discord') checked @endif />
                    <label for="avatar_discord">{{ __('potat0s.form.avatar_discord') }}</label>
                </span>
                <span class="radio">
                    <input id="avatar_gravatar" type="radio" onclick="updateDiscord(false)"
                           name="avatar" value="gravatar" @if(old('avatar', $user->avatar) == 'gravatar') checked @endif />
                    <label for="avatar_gravatar">{{ __('potat0s.form.avatar_gravatar') }}</label>
                </span>
            </div>
            @if($user->avatar == 'discord')
                <a id="discord_button" class="btn btn-discord" href="https://discord.com/api/oauth2/authorize?client_id=824017610763927602&redirect_uri={{ route('discord-callback') }}&response_type=code&scope=identify">
                    <i class="fab fa-discord"></i> {{ __('potat0s.form.parameter.discord_sync') }}
                </a>
            @endif
        </x-form::form-line>
        <x-form::form-line>
            <span class="checkbox">
                <input id="want_newsletter" type="checkbox" name="want_newsletter" value="1" @if(old('want_newsletter', $user->want_newsletter)) checked @endif />
                <label for="want_newsletter">{{ __('potat0s.form.newsletter') }}</label>
                @error('want_newsletter')
                <div class="text-danger">
                    {{ $message }}
                </div>
                @enderror
            </span>
        </x-form::form-line>

        <x-slot name="submitSlot">{{ __('potat0s.form.update')}}</x-slot>
    </x-form::simple-form>
    <hr/>
    <h2>{{ __('potat0s.page.skills') }}</h2>
    <x-form::simple-form formUrl="{{ route('skills-post') }}">
        <div class="skills container row">
            @foreach($skills as $skill)
                <div class="col col-12 col-lg-4">
                    <input type="checkbox" id="skill_{{ $skill->id }}" name="skills[]" value="{{ $skill->id }}" @if(in_array($skill->id, $userSkills)) checked @endif/>
                    <label for="skill_{{ $skill->id }}">
                        <i class="{{ $skill->icon }}"></i>
                        <span>
                        {{ $skill->name }}
                    </span>
                    </label>
                </div>
            @endforeach
        </div>
        <x-slot name="submitSlot">{{ __('potat0s.form.update')}}</x-slot>
    </x-form::simple-form>
    <hr/>
    <h2>Modification du mot de passe</h2>
    <x-form::simple-form formUrl="{{ route('password-update-post') }}">
        <x-form::form-line>
            <label for="actual_password">
                {{ __('potat0s.form.actual_password') }}
            </label>
            @error('actual_password')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input type="password" id="actual_password" name="actual_password">
        </x-form::form-line>
        <x-form::form-line>
            <label for="new_password">
                {{ __('potat0s.form.new_password') }}
            </label>
            @error('new_password')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input type="password" id="new_password" name="new_password">
        </x-form::form-line>
        <x-form::form-line>
            <label for="password_confirm">
                {{ __('potat0s.form.password_confirm') }}
            </label>
            @error('password_confirmation')
            <div class="text-danger">
                {{ $message }}
            </div>
            @enderror
            <input type="password" id="password_confirm" name="password_confirmation">
        </x-form::form-line>

        <x-slot name="submitSlot">{{ __('potat0s.form.update')}}</x-slot>
    </x-form::simple-form>

@endsection


@section('css')
    .radio {
        width: 33%;
        display: inline-block;
        float: left;
        font-size: 20px;
    }

    .radio input{
        width: 17px;
        height: 17px;
    }

    .skills {
        margin: 0;
    }

    .skills .col {
        padding: 0 5px;
    }

    .skills label{
        height: 40px;
        width: 100%;
        line-height: 40px;
        margin: 5px 0;
        background: #820386;
        padding: 0 0 0 5px;
    }

    .skills input[type="checkbox"]:checked+label{
        background: #311f39;
    }

    .skills input[type="checkbox"]{
        display: none;
    }

    .form-line a.btn-discord {
        background: #7289da;
        color: #fff;
    }
@endsection

@push('js')
    <link href="{{ asset('font/all.min.css') }}" rel="stylesheet"/>
    <script>
        function updateDiscord(showIt){
            document.getElementById('discord_button').hidden = !showIt;
        }
    </script>
@endpush
