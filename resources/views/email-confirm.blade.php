@extends('layout.app')

@section('body')
    <h1>{{ __('potat0s.page.validation') }}</h1>
    <x-form::simple-form :formUrl="route('confirm-email-post',[$user->email_token_validation])">
        <x-form::form-line>
            <label for="email">
                {{ __('potat0s.form.register.aim') }}
            </label>
            <input type="text" id="email" name="email" value="{{ $user->email }}" disabled>
        </x-form::form-line>
        <x-form::form-line>
            <label for="password">
                {{ __('potat0s.form.password') }}
            </label>
            <input type="password" id="password" name="password">
        </x-form::form-line>
        <x-slot name="submitSlot">{{ __('potat0s.form.confirm_account')}}</x-slot>
    </x-form::simple-form>
@endsection
