<?php

return [
    'menu' => [
        'planning' => 'Planning',
        'jams' => 'Jams',
        'actually_live' => 'Actuellement en live'
    ],
    'session' => [
        'login' => 'Connexion',
        'register' => 'Inscription',
        'parameters' => 'Paramètres',
        'logout' => 'Déconnexion'
    ],
    'jam' => [
        'no_jam' => 'Pas de jam à venir',
        'edit' => 'Modifier',
        'leaderboard' => 'Classement',
        'leave' => 'Quitter la jam',
        'join' => 'Rejoindre',
        'return' => 'Retour à la jam',
        'default_criteria' => 'Global',
        'criterias' => 'Critères de note',
        'leaderboard_of' => 'Classement de'
    ],
    'game' => [
        'edit' => 'Modifier',
        'play_it' => 'Jouer au jeu',
        'description' => 'Description du jeu',
        'add_note' => 'Editer vos notes',
        'send_or_update_note' => 'Sauvegardes les notes',
        'delete' => 'Supprimer',
        'delete_modal' => 'Confirmation de la suppression',
        'delete_confirm' => 'Êtes vous sûr de vouloir supprimer {0} ?',
        'no' => 'Annuler',
        'go_to_page' => 'Voir la page du jeu',
        'updated' => 'Votre jeu a bien été sauvegardé.',
        'already_exist' => 'Vous avez déjà publié un jeu.'
    ],
    'page' => [
        'game_to_try' => 'Jeux que tu devrais essayer !',
        'news' => 'Actualité',
        'last_topics' => 'Derniers topics du forum',
        'login' => 'Connexion'
    ],
    'form' => [
        'send' => 'Envoyer',
        'login' => [
            'button' => 'Se connecter',
            'remember' => 'Se souvenir de moi',
            'email' => 'Adresse email',
            'password' => 'Mot de passe'
        ],
        'not_published' => 'Non publié',
        'published' => 'Publié',
        'ranked' => 'Classé',
        'not_ranked' => 'Non classé',
        'game' => [
            'title' => 'Titre du jeu',
            'url' => 'Page du jeu',
            'description' => 'Description'
        ],
        'jam' => [
            'title' => 'Titre de la jam',
            'type' => 'Type de jam',
            'theme' => 'Choix du thème',
            'dates' => 'Dates de la jam',
            'start_time' => 'Heure de début',
            'end_time' => 'Heure de fin',
            'end_vote' => 'Heure de fin des votes',
            'description' => 'Description',
            'state' => 'Etat de la publication',
            'criterias' => 'Critères de notation'
        ]
    ]
];
