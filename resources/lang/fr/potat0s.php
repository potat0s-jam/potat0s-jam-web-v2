<?php

return [
    'time' => [
        "day" => "Jour(s)",
        "hour" => "Heure(s)",
        "minute" => "Minute(s)",
        "second" => "Seconde(s)"
    ],
    'menu' => [
        'calendar' => 'Calendrier',
        'jams' => 'Jams',
        'actually_live' => 'Actuellement en live',
        'discord' => 'Serveur Discord'
    ],
    'session' => [
        'login' => 'Connexion',
        'login_success' => 'Vous êtes désormais connecté.',
        'logout_success' => 'Vous avez bien été déconnecté.',
        'update_password_success' => 'Votre mot de passe a bien été mis à jour.',
        'register' => 'Inscription',
        'parameters' => 'Paramètres',
        'logout' => 'Déconnexion',
        'unknown' => 'Utilisateur inconnu.',
        'my_games' => 'Mes jeux',
        'reset'=>[
            'send'=> [
                'success' => 'Votre demande a bien été envoyée. Vous allez recevoir un email sous peu.',
            ],
            'token' => [
                'unknown' => 'Votre demande de modification n\'existe pas.'
            ]
        ]
    ],
    'jam' => [
        'no_jam' => 'Pas de jam à venir',
        'actual_vote' => 'Vote en cours',
        'next_edition' => 'Prochaine édition',
        'on_going' => 'Jam en cours',
        'edit' => 'Modifier',
        'leaderboard' => 'Classement',
        'leave' => 'Quitter la jam',
        'join' => 'Rejoindre',
        'return' => 'Retour à la jam',
        'default_criteria' => 'Global',
        'criterias' => 'Critères de note',
        'leaderboard_of' => 'Classement de',
        'not_ranked' => 'Cette édition n\'est pas classée.',
        'theme_soon' => '<b>Le thème sera révélé au début de la jam.</b>',
        'go_to' => 'Voir la page',
        'login_before_participate' => 'Merci de vous connecter si vous souhaitez participer !',
        'publish' => 'Publier',
        'update' => [
            'success' => 'La Jam a bien été mise à jour !'
        ]
    ],
    'game' => [
        'edit' => 'Modifier',
        'play_it' => 'Jouer au jeu',
        'description' => 'Description du jeu',
        'add_note' => 'Editer vos notes',
        'send_or_update_note' => 'Sauvegardes les notes',
        'delete' => 'Supprimer',
        'delete_modal' => 'Confirmation de la suppression',
        'delete_confirm' => 'Êtes vous sûr de vouloir supprimer {0} ?',
        'no' => 'Annuler',
        'go_to_page' => 'Voir la page du jeu',
        'updated' => 'Votre jeu a bien été sauvegardé.',
        'already_exist' => 'Vous avez déjà publié un jeu.',
        'delete_success' => 'Votre jeu a bien été retiré.'
    ],
    'page' => [
        'game_to_try' => 'Jeux que tu devrais essayer !',
        'news' => 'Actualité',
        'last_topics' => 'Derniers topics du forum',
        'login' => 'Connexion',
        'calendar' => 'Calendrier de la Potat0s Game Jam',
        'validation' => 'Validation du compte',
        'game-list' => 'Liste des jeux de {0}',
        'account' => 'Gestion de mon compte',
        'parameters' => 'Mes paramètres',
        'skills' => 'Mes compétences'
    ],
    'form' => [
        'avatar_no' => 'Aucun',
        'avatar_discord' => 'Discord',
        'avatar_gravatar' => 'Gravatar',
        'email' => 'Email',
        'username' => 'Nom d\'utilisateur',
        'password' => 'Mot de passe',
        'password_confirm' => 'Confirmation du mot de passe',
        'avatar' => 'Avatar',
        'newsletter' => 'Inscription à la newsletter',
        'send' => 'Envoyer',
        'update' => 'Mettre à jour',
        'confirm_account' => 'Valider mon compte',
        'actual_password' => 'Mot de passe actuel',
        'new_password' => 'Nouveau mot de passe',
        'forgot_password' => 'Mot de passe oublié ?',
        'click_here' => 'Cliquez ici',
        'need_account' => 'Besoin d\'un compte ?',
        'click_register' => 'Inscrivez-vous',
        'login' => [
            'button' => 'Se connecter',
            'remember' => 'Se souvenir de moi',
            'email' => 'Adresse email',
            'password' => 'Mot de passe'
        ],
        'not_published' => 'Non publié',
        'published' => 'Publié',
        'ranked' => 'Classé',
        'not_ranked' => 'Non classé',
        'game' => [
            'title' => 'Titre du jeu',
            'url' => 'Page du jeu',
            'vignette' => 'Vignette du jeu',
            'description' => 'Description'
        ],
        'jam' => [
            'title' => 'Titre de la jam',
            'type' => 'Type de jam',
            'theme' => 'Choix du thème',
            'dates' => 'Dates de la jam',
            'start_time' => 'Heure de début',
            'end_time' => 'Heure de fin',
            'end_vote' => 'Heure de fin des votes',
            'description' => 'Description',
            'state' => 'Etat de la publication',
            'criterias' => 'Critères de notation'
        ],
        'dates' => [
            'start' => 'Début de la jam',
            'end' => 'Fin de la jam',
            "end_vote" => "Fin des votes"
        ],
        'register' => [
            'aim' => 'Compte cible',
            'button' => 'S\'inscrire',
            'success' => 'Votre compte a bien été créé. Vérifiez vos mails pour l\'activer.'
        ],
        'parameter' => [
            'success' => 'Vos paramètres ont bien été mis à jour.',
            'skill_success' => 'Vos compétences ont bien été mises à jour.',
            'discord_sync' => 'Synchronisation de l\'avatar',
            'discord_success' => 'Votre image de profil est bien à jour !',
            'discord_failure' => 'Une erreur est survenue lors de la synchronisation avec Discord.',
            'current_pass_invalid' => 'Le mot de passe ne correspond pas à votre mot de passe actuel.',
            'password_updated' => 'Le mot de passe a bien été mis à jour.'
        ]
    ],
    'email' => [
        'confirmation' => 'Confirmation de votre adresse email',
        'reset-password' => 'Demande de réinitialisation du mot de passe'
    ]
];
