<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToForumAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('forum_answer', function(Blueprint $table)
		{
			$table->foreign('user_id', 'forum_answer_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('forum_topic_id', 'forum_answer_ibfk_2')->references('id')->on('forum_topic')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('forum_answer', function(Blueprint $table)
		{
			$table->dropForeign('forum_answer_ibfk_1');
			$table->dropForeign('forum_answer_ibfk_2');
		});
	}

}
