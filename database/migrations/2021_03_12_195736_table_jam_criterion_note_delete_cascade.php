<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TableJamCriterionNoteDeleteCascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->dropForeign('note_ibfk_2');
            $table->foreign('criteria_id','note_ibfk_2')->references('id')->on('jam_criterion')->onUpdate('CASCADE')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->dropForeign('note_ibfk_2');
            $table->foreign('criteria_id', 'note_ibfk_2')->references('id')->on('jam_criterion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
