<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBasicDataToSkillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $models = [
            [
                'id' => 1,
                'name' => 'Développeur',
                'icon' => 'fas fa-code'
            ],
            [
                'id' => 2,
                'name' => 'Artiste 2D',
                'icon' => 'fas fa-pencil-alt'
            ],
            [
                'id' => 3,
                'name' => 'Artiste 3D',
                'icon' => 'fas fa-cube'
            ],
            [
                'id' => 4,
                'name' => 'Musicien',
                'icon' => 'fas fa-music'
            ],
            [
                'id' => 5,
                'name' => 'Sound Designer',
                'icon' => 'fas fa-headphones'
            ],
            [
                'id' => 6,
                'name' => 'Animation',
                'icon' => 'fas fa-walking'
            ],
            [
                'id' => 7,
                'name' => 'Voice Acting',
                'icon' => 'fas fa-microphone'
            ],
        ];

        \Illuminate\Support\Facades\DB::table('skills')->insert($models);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\DB::table('skills')->delete();
    }
}
