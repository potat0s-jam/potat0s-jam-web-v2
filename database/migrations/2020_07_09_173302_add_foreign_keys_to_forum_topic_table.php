<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToForumTopicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('forum_topic', function(Blueprint $table)
		{
			$table->foreign('forum_category_id', 'forum_topic_ibfk_1')->references('id')->on('forum_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('user_id', 'forum_topic_ibfk_2')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('forum_topic', function(Blueprint $table)
		{
			$table->dropForeign('forum_topic_ibfk_1');
			$table->dropForeign('forum_topic_ibfk_2');
		});
	}

}
