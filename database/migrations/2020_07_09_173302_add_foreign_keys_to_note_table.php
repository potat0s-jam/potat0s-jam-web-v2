<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('note', function(Blueprint $table)
		{
			$table->foreign('submission_id', 'note_ibfk_1')->references('id')->on('submission')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('criteria_id', 'note_ibfk_2')->references('id')->on('criterion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('note', function(Blueprint $table)
		{
			$table->dropForeign('note_ibfk_1');
			$table->dropForeign('note_ibfk_2');
		});
	}

}
