<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('field', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nom', 50);
			$table->string('description', 191);
			$table->integer('type_field_id')->index('type_field_id');
			$table->boolean('is_archived');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('field');
	}

}
