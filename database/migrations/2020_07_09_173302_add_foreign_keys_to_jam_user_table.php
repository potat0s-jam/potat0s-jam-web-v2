<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJamUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jam_user', function(Blueprint $table)
		{
			$table->foreign('user_id', 'jam_user_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('jam_id', 'jam_user_ibfk_2')->references('id')->on('jam')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jam_user', function(Blueprint $table)
		{
			$table->dropForeign('jam_user_ibfk_1');
			$table->dropForeign('jam_user_ibfk_2');
		});
	}

}
