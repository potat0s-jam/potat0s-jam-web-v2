<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToJamCriterionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jam_criterion', function (Blueprint $table) {
            $table->integer('id', true)->first();
            $table->string('name', '50');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jam_criterion', function (Blueprint $table) {
            $table->dropColumn('id');
            $table->dropColumn('name');
        });
    }
}
