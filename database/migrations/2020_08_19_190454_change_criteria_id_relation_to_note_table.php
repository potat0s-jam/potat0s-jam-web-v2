<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeCriteriaIdRelationToNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->dropForeign('note_ibfk_2');
            $table->foreign('criteria_id')->references('id')->on('jam_criterion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->dropForeign('note_criteria_id_foreign');
            $table->foreign('criteria_id', 'note_ibfk_2')->references('id')->on('criterion');
        });
    }
}
