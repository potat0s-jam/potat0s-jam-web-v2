<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJamUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jam_user', function(Blueprint $table)
		{
			$table->integer('user_id')->index('user_id');
			$table->integer('jam_id')->index('jam_id');
			$table->primary(['user_id','jam_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jam_user');
	}

}
