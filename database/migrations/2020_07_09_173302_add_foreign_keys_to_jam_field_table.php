<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJamFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jam_field', function(Blueprint $table)
		{
			$table->foreign('jam_id', 'jam_field_ibfk_1')->references('id')->on('jam')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('field_id', 'jam_field_ibfk_2')->references('id')->on('field')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jam_field', function(Blueprint $table)
		{
			$table->dropForeign('jam_field_ibfk_1');
			$table->dropForeign('jam_field_ibfk_2');
		});
	}

}
