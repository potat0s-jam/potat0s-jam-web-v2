<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('field', function(Blueprint $table)
		{
			$table->foreign('type_field_id', 'field_ibfk_1')->references('id')->on('type_field')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('field', function(Blueprint $table)
		{
			$table->dropForeign('field_ibfk_1');
		});
	}

}
