<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubmissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submission', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('url_itchio', 65535);
			$table->string('description', 191);
			$table->dateTime('date_publication');
			$table->integer('user_id')->index('user_id');
			$table->integer('jam_id')->index('jam_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submission');
	}

}
