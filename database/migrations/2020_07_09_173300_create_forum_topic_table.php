<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumTopicTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_topic', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('nom', 75);
			$table->text('content', 65535);
			$table->integer('forum_category_id')->index('forum_category_id');
			$table->dateTime('date');
			$table->integer('user_id')->index('user_id');
			$table->boolean('is_locked');
			$table->boolean('is_pin');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_topic');
	}

}
