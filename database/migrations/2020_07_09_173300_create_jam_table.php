<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jam', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('theme', 50)->nullable();
			$table->boolean('is_ranked');
			$table->string('url_title', 100)->unique('url_title');
			$table->string('title', 100)->unique('title');
			$table->text('description')->nullable();
			$table->dateTime('start_jam');
			$table->dateTime('end_jam');
			$table->dateTime('end_vote')->nullable();
			$table->boolean('is_published');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jam');
	}

}
