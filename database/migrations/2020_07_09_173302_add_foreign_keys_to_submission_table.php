<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSubmissionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submission', function(Blueprint $table)
		{
			$table->foreign('user_id', 'submission_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('jam_id', 'submission_ibfk_2')->references('id')->on('jam')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submission', function(Blueprint $table)
		{
			$table->dropForeign('submission_ibfk_1');
			$table->dropForeign('submission_ibfk_2');
		});
	}

}
