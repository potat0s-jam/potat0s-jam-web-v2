<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSubmissionFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submission_field', function(Blueprint $table)
		{
			$table->foreign('submission_id', 'submission_field_ibfk_1')->references('id')->on('submission')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('field_id', 'submission_field_ibfk_2')->references('id')->on('field')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submission_field', function(Blueprint $table)
		{
			$table->dropForeign('submission_field_ibfk_1');
			$table->dropForeign('submission_field_ibfk_2');
		});
	}

}
