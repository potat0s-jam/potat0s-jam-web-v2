<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsToJamCriterionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jam_criterion', function (Blueprint $table) {
            $table->dropPrimary(['id_jam','id_criterion']);
            $table->dropForeign( 'jam_criterion_ibfk_2');
            $table->dropColumn('id_criterion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jam_criterion', function (Blueprint $table) {
            $table->integer('id_criterion')->index('id_criteria');
            $table->primary(['id_jam','id_criterion']);
            $table->foreign('id_criterion', 'jam_criterion_ibfk_2')->references('id')->on('criterion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
}
