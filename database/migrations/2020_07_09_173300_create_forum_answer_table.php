<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForumAnswerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forum_answer', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('content', 65535);
			$table->dateTime('date');
			$table->integer('user_id')->index('user_id');
			$table->integer('forum_topic_id')->index('forum_topic_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forum_answer');
	}

}
