<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToForumCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('forum_category', function(Blueprint $table)
		{
			$table->foreign('forum_category_id', 'forum_category_ibfk_1')->references('id')->on('forum_category')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('forum_category', function(Blueprint $table)
		{
			$table->dropForeign('forum_category_ibfk_1');
		});
	}

}
