<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUniqueConstraintAtUserSkillsTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_skills', function(Blueprint $table)
        {
           $table->unique(['user_id','skill_id'], 'unique_user_skill_ids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_skills', function(Blueprint $table)
        {
            $table->dropUnique('unique_user_skill_ids');
        });
    }
}
