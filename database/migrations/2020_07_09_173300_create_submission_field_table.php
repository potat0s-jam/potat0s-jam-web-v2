<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubmissionFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submission_field', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('submission_id')->index('submission_id');
			$table->integer('field_id')->index('field_id');
			$table->integer('content');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submission_field');
	}

}
