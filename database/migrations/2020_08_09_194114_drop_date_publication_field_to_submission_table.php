<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropDatePublicationFieldToSubmissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submission', function (Blueprint $table) {
            if(Schema::hasColumn('submission', 'date_publication')){
                $table->dropColumn('date_publication');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submission', function (Blueprint $table) {
            if(!Schema::hasColumn('submission', 'date_publication')){
                $table->dateTime('date_publication')->nullable();
            }
        });
    }
}
