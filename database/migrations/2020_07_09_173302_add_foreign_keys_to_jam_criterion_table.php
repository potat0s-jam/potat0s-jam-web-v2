<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToJamCriterionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('jam_criterion', function(Blueprint $table)
		{
			$table->foreign('id_jam', 'jam_criterion_ibfk_1')->references('id')->on('jam')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_criterion', 'jam_criterion_ibfk_2')->references('id')->on('criterion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('jam_criterion', function(Blueprint $table)
		{
			$table->dropForeign('jam_criterion_ibfk_1');
			$table->dropForeign('jam_criterion_ibfk_2');
		});
	}

}
