<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToNewsTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('news_tag', function(Blueprint $table)
		{
			$table->foreign('tag_id', 'news_tag_ibfk_1')->references('id')->on('tag')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('news_id', 'news_tag_ibfk_2')->references('id')->on('news')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('news_tag', function(Blueprint $table)
		{
			$table->dropForeign('news_tag_ibfk_1');
			$table->dropForeign('news_tag_ibfk_2');
		});
	}

}
