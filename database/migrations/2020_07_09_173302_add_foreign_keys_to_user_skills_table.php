<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToUserSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_skills', function(Blueprint $table)
		{
			$table->foreign('user_id', 'user_skill_ibfk_1')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('skill_id', 'user_skill_ibfk_2')->references('id')->on('skills')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_skills', function(Blueprint $table)
		{
			$table->dropForeign('user_skill_ibfk_1');
			$table->dropForeign('user_skill_ibfk_2');
		});
	}

}
