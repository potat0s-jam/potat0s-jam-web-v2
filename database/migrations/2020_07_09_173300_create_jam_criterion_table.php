<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJamCriterionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jam_criterion', function(Blueprint $table)
		{
			$table->integer('id_jam')->index('id_jam');
			$table->integer('id_criterion')->index('id_criteria');
			$table->primary(['id_jam','id_criterion']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jam_criterion');
	}

}
