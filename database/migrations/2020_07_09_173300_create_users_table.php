<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191);
			$table->string('email', 191);
			$table->dateTime('email_verified_at')->nullable();
			$table->string('password', 191);
			$table->string('discord_id', 50)->nullable();
			$table->boolean('is_admin')->default(0);
			$table->string('avatar', 10)->default('no');
			$table->boolean('want_newsletter');
			$table->timestamps();
			$table->string('remember_token', 100)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
