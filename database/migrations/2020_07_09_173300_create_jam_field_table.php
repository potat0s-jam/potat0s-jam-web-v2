<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateJamFieldTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('jam_field', function(Blueprint $table)
		{
			$table->integer('jam_id')->index('jam_id');
			$table->integer('field_id')->index('field_id');
			$table->primary(['jam_id','field_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('jam_field');
	}

}
